package com.nael.tremendous.utils;

import com.apollo.World;

public class WorldManager {

    private static WorldManager _instance = new WorldManager();
    private World world;

    private WorldManager() {
        world = new World();
    }

    public final static WorldManager getInstance() {
        return _instance;
    }

    public World getWorld() {
        return world;
    }
}
