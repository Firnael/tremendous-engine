package com.nael.tremendous.utils.network;

import com.apollo.Entity;
import com.apollo.utils.Bag;
import com.esotericsoftware.kryo.Kryo;
import com.esotericsoftware.kryonet.EndPoint;
import org.newdawn.slick.geom.Vector2f;

import java.util.HashMap;
import java.util.Map;

public class Network {

    public static final int TCP = 54666;
    public static final int UDP = 54777;

    static public void register(EndPoint endPoint) {
        Kryo kryo = endPoint.getKryo();

        // Java Core classes
        kryo.register(Vector2f.class);
        kryo.register(Vector2f[].class);
        kryo.register(HashMap.class);
        kryo.register(int[].class);
        kryo.register(int[][].class);
        kryo.register(String[].class);

        // Apollo classes
        kryo.register(Bag.class);
        kryo.register(Entity.class);

        // Custom classes
        kryo.register(TestMessage.class);
        kryo.register(NewPlayer.class);
        kryo.register(GetPlayers.class);
        kryo.register(RemovePlayer.class);
        kryo.register(PlayerInput.class);
        kryo.register(GetMobs.class);
        kryo.register(NewMob.class);
        kryo.register(AIAttack.class);
        kryo.register(GetTerrain.class);
        kryo.register(NewTerrain.class);
        kryo.register(HeartBeat.class);
    }

    /* Utils */
    static public class TestMessage {
        public String message;
    }




    /* Players */
    static public class GetPlayers {
        int playerCount;
        Vector2f[] positions;
        String[] networkIds;
    }

    static public class NewPlayer {
        public String networkId;
        public Vector2f position;
        public boolean isYou;
    }

    static public class RemovePlayer {
        public String networkId;
    }

    static public class PlayerInput {
        public String networkId;
        public Map<Integer, Boolean> keyStates;
        public Vector2f mousePosition;
    }

    /* Mobs */
    static public class GetMobs {
        public int mobCount;
        public Vector2f[] positions;
        public String[] networkIds;
        public String[] goalIds;
        public int[] viewRanges;
    }

    static public class NewMob {
        public String networkId;
        public Vector2f position;

        public boolean seeking;
        public String goalId;
        public int viewRange;
    }

    static public class AIAttack {
        public String networkId;
        public Vector2f norm;
    }

    /* Terrain */
    static public class GetTerrain {
    }

    static public class NewTerrain {
        int[][] array;
    }

    static public class HeartBeat {
        public Map<String, Vector2f> playersPositions;
        public Map<String, Vector2f> mobsPositions;
        public Map<String, Vector2f> mobsSpeeds;
    }
}
