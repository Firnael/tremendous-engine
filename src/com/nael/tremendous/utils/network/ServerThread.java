package com.nael.tremendous.utils.network;

import org.newdawn.slick.util.Log;

public class ServerThread extends Thread {

    private long heartBeatInterval = 50L;
    private long lastUpdateTime;

    public ServerThread() {
        this.lastUpdateTime = System.currentTimeMillis();
        Log.info("[SERVER] - Heartbeat started - popom, popom ~");
    }

    public void run() {
        while (this.isAlive()) {
            long currentTime = System.currentTimeMillis();
            if (currentTime - lastUpdateTime >= heartBeatInterval) {
                lastUpdateTime = currentTime;

                // HEARTBEAT
                NetworkManager.getInstance().serverHeartBeat();
            }
        }
    }
}
