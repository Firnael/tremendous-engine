package com.nael.tremendous.utils.network;

import com.apollo.Entity;
import com.apollo.World;
import com.apollo.components.Transform;
import com.esotericsoftware.kryonet.Client;
import com.esotericsoftware.kryonet.Connection;
import com.esotericsoftware.kryonet.FrameworkMessage;
import com.esotericsoftware.kryonet.Listener;
import com.esotericsoftware.kryonet.Server;
import com.nael.tremendous.components.AttackComponent;
import com.nael.tremendous.components.InputComponent;
import com.nael.tremendous.components.NetworkIdComponent;
import com.nael.tremendous.components.SpeedComponent;
import com.nael.tremendous.components.WeaponComponent;
import com.nael.tremendous.components.ai.ArrivalMovementComponent;
import com.nael.tremendous.components.ai.SeekMovementComponent;
import com.nael.tremendous.managers.MobManager;
import com.nael.tremendous.managers.PlayerManager;
import com.nael.tremendous.managers.TerrainManager;
import com.nael.tremendous.managers.WeaponManager;
import com.nael.tremendous.utils.WorldManager;
import org.newdawn.slick.geom.Vector2f;
import org.newdawn.slick.util.Log;

import java.io.IOException;
import java.util.HashMap;

public class NetworkManager {

    private static NetworkManager _instance = new NetworkManager();
    private World world;
    private Server server;
    private Client client;
    private boolean isServer = true;
    private boolean readyForHeartBeat = false;
    private float latency = 0f;

    private NetworkManager() {
        world = WorldManager.getInstance().getWorld();

        server = new Server() {
            protected Connection newConnection() {
                return new TremendousConnection();
            }
        };

        client = new Client();
    }

    public final static NetworkManager getInstance() {
        return _instance;
    }

    public Server getServer() {
        return server;
    }

    public Client getClient() {
        return client;
    }

    public boolean isServer() {
        return isServer;
    }

    public void setReadyForHeartBeat(boolean value) {
        readyForHeartBeat = value;
    }

    public float getLatency() {
        return latency;
    }

    // ===

    public void initServer() {
        Network.register(server);

        server.addListener(new Listener() {
            public void received(Connection connection, Object object) {

                TremendousConnection c = (TremendousConnection) connection;

                // TestMessage
                if (object instanceof Network.TestMessage) {
                    Network.TestMessage testMessage = (Network.TestMessage) object;
                    Log.info("[SERVER] - Received TestMessage from : " + c.getID() + ", " + testMessage.message);
                }

                // NewPlayer
                if (object instanceof Network.NewPlayer) {
                    Log.info("[SERVER] - Received NewPlayer from " + c.getID());

                    if (c.playerInit) {
                        Log.warn("[SERVER] - Client " + c.getID() + " already sent a NewPlayer request");
                        return; // do nothing, player already initialized
                    }
                    else {
                        // Create the new player
                        PlayerManager pm = world.getManager(PlayerManager.class);
                        boolean isMe = false;
                        if(c.getID() == client.getID()) {
                            isMe = true; // Special case, when the server itself creates his player
                        }
                        Entity player = pm.addPlayer(new Vector2f(42.f, 42.f), isMe);
                        player.setComponent(new NetworkIdComponent("Player@" + c.getID()));

                        // Notify clients
                        Network.NewPlayer newPlayerClient = new Network.NewPlayer();
                        server.sendToAllTCP(newPlayerClient);

                        c.playerInit = true;
                    }
                }

                // GetPlayers
                if (object instanceof Network.GetPlayers) {
                    Log.info("[SERVER] - Received GetPlayers from " + c.getID());
                    Network.GetPlayers getPlayers = (Network.GetPlayers) object;
                    PlayerManager pm = world.getManager(PlayerManager.class);
                    int playerCount = pm.getPlayerEntities().size();
                    Vector2f[] positions = new Vector2f[playerCount];
                    String[] networkIds = new String[playerCount];

                    int index = 0;
                    for (Entity e : pm.getPlayerEntities()) {
                        Transform tc = e.getComponent(Transform.class);
                        positions[index] = new Vector2f(tc.getX(), tc.getY());
                        networkIds[index] = e.getComponent(NetworkIdComponent.class).getId();
                        index++;
                    }

                    getPlayers.playerCount = playerCount;
                    getPlayers.positions = positions;
                    getPlayers.networkIds = networkIds;
                    server.sendToTCP(c.getID(), getPlayers);
                    Log.info("[SERVER] - Delivering GetPlayers to " + c.getID());
                }

                // PlayerInput
                if (object instanceof Network.PlayerInput) {
                    Network.PlayerInput playerInput = (Network.PlayerInput) object;
                    Log.info("[SERVER] - Received PlayerInput from " + c.getID());
                    playerInput.networkId = "Player@" + c.getID();

                    // Send to all except the emmiter
                    server.sendToAllExceptTCP(c.getID(), playerInput);
                }

                // GetMobs
                if (object instanceof Network.GetMobs) {
                    Network.GetMobs getMobs = (Network.GetMobs) object;
                    Log.info("[SERVER] - Received GetMobs from " + c.getID());

                    MobManager mm = world.getManager(MobManager.class);
                    int mobCount = mm.getMobEntities().size();
                    Vector2f[] positions = new Vector2f[mobCount];
                    String[] networkids = new String[mobCount];
                    String[] goalIds = new String[mobCount];
                    int[] viewRanges = new int[mobCount];

                    int index = 0;
                    for (Entity e : mm.getMobEntities()) {
                        Transform tc = e.getComponent(Transform.class);
                        positions[index] = new Vector2f(tc.getX(), tc.getY());
                        networkids[index] = e.getComponent(NetworkIdComponent.class).getId();

                        SeekMovementComponent smc = e.getComponent(SeekMovementComponent.class);
                        String goalId = null;
                        int viewRange = 0;
                        if(smc != null) {
                            goalId = smc.getGoal().getComponent(NetworkIdComponent.class).getId();
                            viewRange = smc.getViewRange();
                        }
                        goalIds[index] = goalId;
                        viewRanges[index] = viewRange;

                        index++;
                    }

                    getMobs.mobCount = mobCount;
                    getMobs.positions = positions;
                    getMobs.networkIds = networkids;
                    getMobs.goalIds = goalIds;
                    getMobs.viewRanges = viewRanges;
                    server.sendToTCP(c.getID(), getMobs);
                    Log.info("[SERVER] - Delivering GetMob to " + c.getID());
                }

                // GetTerrain
                if (object instanceof Network.GetTerrain) {
                    Network.GetTerrain getTerrain = (Network.GetTerrain) object;
                    Log.info("[SERVER] - Received GetTerrain from " + c.getID());

                    TerrainManager tm = world.getManager(TerrainManager.class);
                    Network.NewTerrain newTerrain = new Network.NewTerrain();
                    newTerrain.array = tm.getTerrainAsArray();

                    server.sendToTCP(c.getID(), newTerrain);
                }
            }

            public void disconnected(Connection connection) {
                TremendousConnection c = (TremendousConnection) connection;

                Network.RemovePlayer removePlayer = new Network.RemovePlayer();
                removePlayer.networkId = "Player@" + c.getID();
                server.sendToAllExceptTCP(c.getID(), removePlayer);
                Log.info("[SERVER] - Client with id " + c.getID() + " disconnected.");
            }
        });

        // Start the server
        Log.info("[SERVER] - Starting...");
        server.start();
        Log.info("[SERVER] - Started");

        // Server connection
        try {
            Log.info("[SERVER] - Binding...");
            server.bind(Network.TCP, Network.UDP);
            Log.info("[SERVER] - Bound on ports TCP : " + Network.TCP + ", UDP : " + Network.UDP);
        } catch (IOException e) {
            Log.error("[SERVER] - Could not bind on ports TCP : " + Network.TCP + ", UDP : " + Network.UDP);
            e.printStackTrace();
            System.exit(1);
        }

    }

    public void initClient() {
        Network.register(client);

        client.addListener(new Listener.ThreadedListener(new Listener() {
            public void connected(Connection connection) {
                Log.info("[CLIENT] - Connected");
                client.updateReturnTripTime();
            }

            public void disconnected(Connection connection) {
                Log.info("[CLIENT] - Disconnected");
            }

            public void received(Connection connection, Object object) {
                // Ping reply
                if (object instanceof FrameworkMessage.Ping) {
                    FrameworkMessage.Ping ping = (FrameworkMessage.Ping)object;
                    if (ping.isReply) {
                        latency = connection.getReturnTripTime();
                    }
                }

                // TestMessage
                if (object instanceof Network.TestMessage) {
                    Network.TestMessage testMessage = (Network.TestMessage) object;
                    Log.info("[CLIENT] - TestMessage : " + testMessage.message);
                }

                // NewPlayer
                if (object instanceof Network.NewPlayer) {
                    Log.info("[CLIENT] - Received NewPlayer handshake");
                    Network.GetPlayers getPlayers = new Network.GetPlayers();
                    client.sendTCP(getPlayers);
                    Log.info("[CLIENT] - Sending GetPlayers");
                }

                // GetPlayers
                if (object instanceof Network.GetPlayers) {
                    Network.GetPlayers getPlayers = (Network.GetPlayers) object;
                    Log.info("[CLIENT] - Received GetPlayers");

                    PlayerManager pm = world.getManager(PlayerManager.class);
                    for(int i=0; i<getPlayers.playerCount; i++) {
                        String id = getPlayers.networkIds[i];
                        Entity e = pm.getPlayerEntityByNetworkId(id);
                        if(e == null) {
                            boolean isMe = false;
                            if(id.equals("Player@" + client.getID())) {
                                isMe = true;
                            }
                            Entity player = pm.addPlayer(getPlayers.positions[i], isMe);
                            player.setComponent(new NetworkIdComponent(id));
                        }
                    }

                    pm.initiated = true;
                    Log.info("[CLIENT] - All PLAYERS loaded");
                }

                // RemovePlayer
                if (object instanceof Network.RemovePlayer) {
                    Network.RemovePlayer removePlayer = (Network.RemovePlayer) object;

                    PlayerManager pm = world.getManager(PlayerManager.class);
                    pm.removePlayer(removePlayer.networkId);

                    Log.info("[CLIENT] - RemovePlayer : ID = " + removePlayer.networkId);
                }

                // PlayerInput
                if (object instanceof Network.PlayerInput) {
                    Network.PlayerInput playerInput = (Network.PlayerInput) object;

                    PlayerManager pm = world.getManager(PlayerManager.class);
                    Entity player = pm.getPlayerEntityByNetworkId(playerInput.networkId);
                    InputComponent ic = player.getComponent(InputComponent.class);
                    ic.setKeyStates(playerInput.keyStates);
                    ic.setMousePosition(playerInput.mousePosition);

                    Log.info("[CLIENT] - PlayerInput : ID = " + playerInput.networkId);
                }

                // GetMobs
                if (object instanceof Network.GetMobs) {
                    Network.GetMobs getMobs = (Network.GetMobs) object;
                    Log.info("[CLIENT] - Received GetMobs");

                    MobManager mm = world.getManager(MobManager.class);
                    for(int i=0; i<getMobs.mobCount; i++) {
                        String id = getMobs.networkIds[i];
                        Entity e = mm.getMobEntityByNetworkId(id);
                        if(e == null) {
                            Entity mob = mm.addMob(getMobs.positions[i]);
                            mob.setComponent(new NetworkIdComponent(id));

                            if(getMobs.goalIds[i] != null) {
                                Entity goal = world.getManager(PlayerManager.class).getPlayerEntityByNetworkId(getMobs.goalIds[i]);
                                mob.setComponent(new SeekMovementComponent(goal, getMobs.viewRanges[i]));
                                mob.setComponent(new ArrivalMovementComponent(goal));
                            }
                        }
                    }

                    mm.initiated = true;
                    Log.info("[CLIENT] - All MOBS loaded");
                }

                // NewMob
                if (object instanceof Network.NewMob) {
                    Network.NewMob newMob = (Network.NewMob) object;

                    MobManager mm = world.getManager(MobManager.class);
                    Entity mob = mm.addMob(newMob.position);
                    mob.setComponent(new NetworkIdComponent(newMob.networkId));

                    if(newMob.seeking) {
                        Entity goal = world.getManager(PlayerManager.class).getPlayerEntityByNetworkId(newMob.goalId);
                        mob.setComponent(new SeekMovementComponent(goal, newMob.viewRange));
                        mob.setComponent(new ArrivalMovementComponent(goal));
                    }

                    Log.info("[CLIENT] - NewMob : ID = " + newMob.networkId);
                }

                // AIAttack
                if(object instanceof Network.AIAttack) {
                    Network.AIAttack aiAttack = (Network.AIAttack) object;
                    Log.info("[CLIENT] - AIAttack : ID = " + aiAttack.networkId);

                    try {
                        Entity ai = world.getManager(MobManager.class).getMobEntityByNetworkId(aiAttack.networkId);
                        Entity weapon = world.getManager(WeaponManager.class).addWeapon(WeaponComponent.WeaponType.SPEAR, ai, aiAttack.norm);
                        AttackComponent aiatc = ai.getComponent(AttackComponent.class);
                        aiatc.setWeapon(weapon);
                        aiatc.setAttacking(true);
                    }
                    catch(Exception e) {
                        Log.info("[CLIENT] - WARNING : AIAttack, can't find AI with NID = " + aiAttack.networkId);
                    }
                }

                // NewTerrain
                if (object instanceof Network.NewTerrain) {
                    Log.info("[CLIENT] - NewTerrain");
                    Network.NewTerrain newTerrain = (Network.NewTerrain) object;

                    TerrainManager tm = world.getManager(TerrainManager.class);
                    tm.generateTerrainFromArray(newTerrain.array);
                }

                // Heartbeat
                if (object instanceof Network.HeartBeat) {
                    if(!readyForHeartBeat) {
                        return;
                    }

                    Network.HeartBeat heartBeat = (Network.HeartBeat) object;
                    PlayerManager pm = world.getManager(PlayerManager.class);
                    MobManager mm = world.getManager(MobManager.class);

                    // Players
                    for(String networkId : heartBeat.playersPositions.keySet()) {
                        Vector2f pos = heartBeat.playersPositions.get(networkId);
                        Entity e = pm.getPlayerEntityByNetworkId(networkId);

                        if(e != null) {
                            Transform tc = e.getComponent(Transform.class);
                            tc.setLocation(pos.getX(), pos.getY());
                        }
                    }

                    // Mobs
                    for(String networkId : heartBeat.mobsPositions.keySet()) {
                        Vector2f pos = heartBeat.mobsPositions.get(networkId);
                        Vector2f speed = heartBeat.mobsSpeeds.get(networkId);
                        Entity e = mm.getMobEntityByNetworkId(networkId);

                        if(e != null) {
                            Transform tc = e.getComponent(Transform.class);
                            tc.setLocation(pos.getX(), pos.getY());
                            e.getComponent(SpeedComponent.class).setVectors(speed.getX(), speed.getY());

                            /*
                            float distance = tc.getDistanceTo(pos.getX(), pos.getY());
                            if(distance > e.getComponent(BodyComponent.class).getBox().getBoundingCircleRadius()) {
                                Vector2f norm = new Vector2f(pos.getX() - tc.getX(), pos.getY() - tc.getY()).normalise();

                                tc.setLocation(pos.getX(), pos.getY());
                            }
                            */
                        }
                    }
                }
            }
        }));

        // Start the client
        client.start();

        // Client connection
        int connectionTimeout = 5000;
        // InetAddress serverAddress = client.discoverHost(Network.TCP, Network.UDP);
        String serverAddress = "127.0.0.1";

        try {
            Log.info("[CLIENT] - Connecting to server at " + serverAddress + "...");
            client.connect(connectionTimeout, serverAddress, Network.TCP, Network.UDP);
        } catch (IOException e) {
            Log.error("[CLIENT] - Could not connect to server at " + serverAddress + " on ports TCP = " + Network.TCP + ", UDP = " + Network.UDP);
            e.printStackTrace();
            System.exit(1);
        }
    }

    public void serverHeartBeat() {
        // Update ping
        client.updateReturnTripTime();

        PlayerManager pm = world.getManager(PlayerManager.class);
        MobManager mm = world.getManager(MobManager.class);

        Network.HeartBeat heartBeat = new Network.HeartBeat();
        HashMap<String, Vector2f> playersPositions = new HashMap<String, Vector2f>();
        HashMap<String, Vector2f> playersSpeeds = new HashMap<String, Vector2f>();
        HashMap<String, Vector2f> mobsPositions = new HashMap<String, Vector2f>();
        HashMap<String, Vector2f> mobsSpeeds = new HashMap<String, Vector2f>();

        for(Entity e : pm.getPlayerEntities()) {
            String networkId = e.getComponent(NetworkIdComponent.class).getId();
            Transform tm = e.getComponent(Transform.class);
            playersPositions.put(networkId, new Vector2f(tm.getX(), tm.getY()));
        }
        heartBeat.playersPositions = playersPositions;

        for(Entity e : mm.getMobEntities()) {
            String networkId = e.getComponent(NetworkIdComponent.class).getId();
            Transform tm = e.getComponent(Transform.class);
            mobsPositions.put(networkId, new Vector2f(tm.getX(), tm.getY()));
            SpeedComponent sc = e.getComponent(SpeedComponent.class);
            mobsSpeeds.put(networkId, new Vector2f(sc.getVx(), sc.getVx()));
        }
        heartBeat.mobsPositions = mobsPositions;
        heartBeat.mobsSpeeds = mobsSpeeds;

        server.sendToAllExceptTCP(NetworkManager.getInstance().client.getID(), heartBeat);
    }

    static class TremendousConnection extends Connection {
        boolean playerInit = false;
    }
}
