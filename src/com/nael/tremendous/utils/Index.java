package com.nael.tremendous.utils;

// Utile pour la manipulation d'index (couples "x,y")
public class Index {
    private int x;
    private int y;

    public Index(int x, int y) {
        this.x = x;
        this.y = y;
    }

    public int getX() {
        return x;
    }

    public int getY() {
        return y;
    }

    public boolean equals(Index index) {
        if(x == index.getX() && y == index.getY()) {
            return true;
        }
        return false;
    }
}