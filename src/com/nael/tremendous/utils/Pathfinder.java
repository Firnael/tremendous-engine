package com.nael.tremendous.utils;

import com.apollo.Entity;
import com.apollo.utils.Bag;
import com.nael.tremendous.components.BodyComponent;
import com.nael.tremendous.components.ai.SeekMovementComponent;
import com.nael.tremendous.components.IndexComponent;
import com.nael.tremendous.managers.TerrainManager;
import org.newdawn.slick.geom.Line;
import org.newdawn.slick.geom.Shape;
import org.newdawn.slick.geom.Vector2f;

import java.util.ArrayList;

public class Pathfinder {

    private static Pathfinder _instance = new Pathfinder();
    private ArrayList<Shape> visualFeedback;
    private float smoothCornerRadius = 30.f;

    private Pathfinder() {
    }

    public final static Pathfinder getInstance() {
        return _instance;
    }

    public Entity getNextNode(Entity searchingEntity) {
        SeekMovementComponent segmc = searchingEntity.getComponent(SeekMovementComponent.class);
        Entity goalEntity = segmc.getGoal();
        Entity node = goalEntity;

        if(node == null) {
            return null;
        }

        TerrainManager tm = WorldManager.getInstance().getWorld().getManager(TerrainManager.class);
        Bag<Entity> terrainEntities = tm.getTerrainEntities();

        Vector2f posStart = searchingEntity.getComponent(BodyComponent.class).getCenter();
        Vector2f posEnd = goalEntity.getComponent(BodyComponent.class).getCenter();

        visualFeedback = new ArrayList<Shape>();
        Line line = new Line(posStart, posEnd);

        boolean straight = true;

        // First check is straight line is possible
        for(Entity te : tm.getImmutableTerrainEntities()) {
            Shape immutableBox = te.getComponent(BodyComponent.class).getBox();
            if(immutableBox.intersects(line)) {
                straight = false;
                break;
            }
        }

        // Straight line toward goal, no need for pathfinding
        if(straight) {
            visualFeedback.add(line);
            return node;
        }

        // If straight line isn't possible, here comes the trouble
        visualFeedback.clear();
        Bag<Entity> goals = new Bag<Entity>();
        Entity closestToSearching = null;
        Entity closestToGoal = null;
        float distanceToSearching = 999.f;
        float distanceToGoal = 999.f;

        // Loop over all terrain entities
        for(Entity te : terrainEntities) {
            BodyComponent teBc = te.getComponent(BodyComponent.class);

            // Save closests terrain entity for later pathfinding
            if(!teBc.isImmutable()) {
                // searching
                if(closestToSearching == null) {
                    closestToSearching = te;
                }
                else {
                    float distance = teBc.getCenter().distance(searchingEntity.getComponent(BodyComponent.class).getCenter());
                    if(distance < distanceToSearching) {
                        distanceToSearching = distance;
                        closestToSearching = te;
                    }
                }
                // goal
                if(closestToGoal == null) {
                    closestToGoal = te;
                }
                else {
                    float distance = teBc.getCenter().distance(goalEntity.getComponent(BodyComponent.class).getCenter());
                    if(distance < distanceToGoal) {
                        distanceToGoal = distance;
                        closestToGoal = te;
                    }
                }
            }
        }

        int width = tm.getWidth();
        int height = tm.getHeight();
        int[][] map = new int[width][height];

        // Init the array representing the map with -1 values
        for(Entity te : terrainEntities) {
            BodyComponent bc = te.getComponent(BodyComponent.class);
            IndexComponent ic = te.getComponent(IndexComponent.class);
            int x = ic.getX();
            int y = ic.getY();

            if(bc.isImmutable()) {
                map[x][y] = -2;
            }
            else {
                map[x][y] = -1;
            }
        }

        // Set the origin to 0
        IndexComponent startIndex = closestToSearching.getComponent(IndexComponent.class);
        map[startIndex.getX()][startIndex.getY()] = 0;

        // Set the goal to -3
        IndexComponent goalIndex = closestToGoal.getComponent(IndexComponent.class);
        map[goalIndex.getX()][goalIndex.getY()] = -3;

        // Create the heatmap
        boolean done = false;
        int currentValue = 0;
        do {
            for(int i=0; i < width; i++) {
                for(int j=0; j < height; j++) {
                    // Get the tile with the current value
                    done = setHeatMapValues(width, height, map, currentValue, i, j);
                    if(done) {  break; }
                }

                if(done) { break; }
            }

            currentValue++;

            if(currentValue > segmc.getViewRange()) {
                return null;
            }

        } while(!done);

        // Get goal list
        boolean pathFound = false;
        int currentNodeValue = currentValue;
        Index currentNodeIndex = new Index(goalIndex.getX(), goalIndex.getY());
        Index startNodeIndex = new Index(startIndex.getX(), startIndex.getY());

        ArrayList<Index> indexList = new ArrayList<Index>();
        indexList.add(currentNodeIndex);

        do {
            Index nextNodeIndex = getNextNodeIndex(width, height, map, currentNodeValue, currentNodeIndex);
            indexList.add(nextNodeIndex);
            currentNodeIndex = nextNodeIndex;

            if(currentNodeIndex.equals(startNodeIndex)) {
                pathFound = true;
            }

        } while(!pathFound);

        // Get entities from indexes
        for(Index index : indexList) {
            goals.add(tm.getEntityByIndex(index.getX(), index.getY()));
        }

        // Optimize goals
        goals = optimizeGoals(tm, goals);

        /*
        for(Index index : indexList) {
            map[index.getX()][index.getY()] = -4;
        }
        printHeatMap(width, height, map);
        */

        // Get real next node
        Entity goalMinusOne = goals.get(goals.size() -1);
        node = goalMinusOne;

        if(goals.size() >= 2) {
            Entity goalMinusTwo = goals.get(goals.size() -2);
            BodyComponent bcgmt = goalMinusTwo.getComponent(BodyComponent.class);
            float distance = posStart.distance(bcgmt.getCenter());

            if(distance < smoothCornerRadius) {
                node = goalMinusTwo;
            }
        }

        // DEBUG : Display
        for (int i = 0; i < goals.size(); i++) {
            visualFeedback.add(goals.get(i).getComponent(BodyComponent.class).getBox());
        }

        return node;
    }

    public boolean setHeatMapValues(int w, int h, int[][] map, int currentValue, int i, int j) {

        boolean goalFound = false;
        ArrayList<Integer> values = new ArrayList<Integer>();

        if(map[i][j] == currentValue) {
            // Check if we don't go outside of the array
            if(i < w-1) {
                map[i+1][j] = calculateValue(map[i+1][j], currentValue); // E
                values.add(map[i+1][j]);
            }
            if(i > 0) {
                map[i-1][j] = calculateValue(map[i-1][j], currentValue); // W
                values.add(map[i-1][j]);
            }

            if(j > 0) {
                map[i][j-1] = calculateValue(map[i][j-1], currentValue); // N
                values.add(map[i][j-1]);
            }
            if(j < h-1) {
                map[i][j+1] = calculateValue(map[i][j+1], currentValue); // S
                values.add(map[i][j+1]);
            }
        }

        // Check if we found the goal (-3)
        for(Integer value : values) {
            if(value == -3) {
                goalFound = true;
                break;
            }
        }

        return goalFound;
    }

    public int calculateValue(int valueToSet, int currentValue) {
        int value = -2;

        // If not a wall
        if(valueToSet != -2) {

            // If never set before
            if(valueToSet == -1) {
                value = currentValue +1;
            }
            else if (valueToSet > currentValue) {
                value = currentValue+1;
            }
            else {
                value = valueToSet;
            }
        }
        return value;
    }

    public Index getNextNodeIndex(int w, int h, int[][] map, int currentNodeValue, Index currentNodeIndex) {
        int x = 0;
        int y = 0;
        int i = currentNodeIndex.getX();
        int j = currentNodeIndex.getY();
        int smallerValue = currentNodeValue;

        // Check if we don't go outside of the array
        if(i < w-1) {
            if(map[i+1][j] >= 0 && map[i+1][j] < smallerValue) { // E
                smallerValue = map[i+1][j];
                x = i+1;
                y = j;
            }

            if(j > 0) {
                if(map[i+1][j-1] >= 0 && map[i+1][j-1] < smallerValue) { // NE
                    smallerValue = map[i+1][j-1];
                    x = i+1;
                    y = j-1;
                }
            }

            if(j < h-1) {
                if(map[i+1][j+1] >= 0 && map[i+1][j+1] < smallerValue) { // SE
                    smallerValue = map[i+1][j+1];
                    x = i+1;
                    y = j+1;
                }
            }
        }
        if(i > 0) {
            if(map[i-1][j] >= 0 && map[i-1][j] < smallerValue) { // W
                smallerValue = map[i-1][j];
                x = i-1;
                y = j;
            }

            if(j > 0) {
                if(map[i-1][j-1] >= 0 && map[i-1][j-1] < smallerValue) {  // NW
                    smallerValue = map[i-1][j-1];
                    x = i-1;
                    y = j-1;
                }
            }
            if(j < h-1) {
                if(map[i-1][j+1] >= 0 && map[i-1][j+1] < smallerValue) { // SW
                    smallerValue = map[i-1][j+1];
                    x = i-1;
                    y = j+1;
                }
            }
        }

        if(j > 0) {
            if(map[i][j-1] >= 0 && map[i][j-1] < smallerValue) { // N
                smallerValue = map[i][j-1];
                x = i;
                y = j-1;
            }
        }
        if(j < h-1) {
            if(map[i][j+1] >= 0 && map[i][j+1] < smallerValue) { // S
                smallerValue = map[i][j+1];
                x = i;
                y = j+1;
            }
        }

        Index nextNodeIndex = new Index(x, y);
        return nextNodeIndex;
    }

    public Bag<Entity> optimizeGoals(TerrainManager tm, Bag<Entity> goals) {
        Bag<Entity> immutableEntities = tm.getImmutableTerrainEntities();
        Bag<Entity> optimizedGoals = new Bag<Entity>();
        // Add the first
        optimizedGoals.add(goals.get(0));

        int globalIndex = 0;
        int currentIndex = globalIndex;
        do {
            Entity currentGoal = goals.get(currentIndex);
            Entity nextGoal = goals.get(globalIndex + 1);

            BodyComponent cgbc = currentGoal.getComponent(BodyComponent.class);
            BodyComponent ngbc = nextGoal.getComponent(BodyComponent.class);

            Line line = new Line(cgbc.getCenter(), ngbc.getCenter());

            boolean broke = false;
            for(Entity ie : immutableEntities) {
                BodyComponent iebc = ie.getComponent(BodyComponent.class);
                if(line.intersects(iebc.getBox())) {
                    broke = true;
                    break;
                }
            }

            if(broke) {
                // Collision in corner, force passage
                if(currentIndex == globalIndex) {
                    globalIndex++;

                    // Add intermediate goal
                    IndexComponent icc = goals.get(currentIndex).getComponent(IndexComponent.class);
                    IndexComponent icg = goals.get(globalIndex).getComponent(IndexComponent.class);
                    Index id1 = new Index(icc.getX(), icg.getY());
                    Index id2 = new Index(icg.getX(), icc.getY());

                    if(tm.getEntityByIndex(id1.getX(), id1.getY()).getComponent(BodyComponent.class).isImmutable()) {
                        optimizedGoals.add(tm.getEntityByIndex(id2.getX(), id2.getY()));
                    }
                    else {
                        optimizedGoals.add(tm.getEntityByIndex(id1.getX(), id1.getY()));
                    }
                }
                else {
                    optimizedGoals.add(goals.get(globalIndex));
                }

                currentIndex = globalIndex;
            }
            else {
                globalIndex++;
            }

        } while(globalIndex < goals.size()-1);

        return optimizedGoals;
    }

    public void printHeatMap(int w, int h, int[][] map) {

        for(int j=0; j < h; j++) {
            for(int i=0; i < w; i++) {
                if(map[i][j] == -4) {
                    System.out.print(" @");
                }
                else if(map[i][j] == -3) {
                    System.out.print(" G");
                }
                else if(map[i][j] == -2) {
                    System.out.print(" #");
                }
                else if(map[i][j] == -1) {
                    System.out.print(" .");
                }
                else if(map[i][j] == 0) {
                    System.out.print(" S");
                }
                else {
                    System.out.print(" .");
                }
                System.out.print(" ");
            }
            System.out.print("\n");
        }
        System.out.println(" = = = = = = = = = = = = = = =");
    }

    public ArrayList<Shape> getVisualFeedback() {
        return visualFeedback;
    }
}
