package com.nael.tremendous.utils;

import org.newdawn.slick.Input;
import org.newdawn.slick.geom.Vector2f;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class InputManager {

    private static InputManager _instance = new InputManager();
    private Map<Binds, Integer> keyBindings;
    private Map<Integer, Boolean> keyStates;
    private List<Integer> changedKeys;
    private Vector2f mousePosition;

    public final static InputManager getInstance() {
        return _instance;
    }

    // Binds Enum
    public enum Binds {
        PLAYER_MOVE_UP,
        PLAYER_MOVE_LEFT,
        PLAYER_MOVE_DOWN,
        PLAYER_MOVE_RIGHT,
        PLAYER_DASH,
        PLAYER_ATTACK,
        PLAYER_DEFENCE;
    }

    private InputManager() {
        keyBindings = new HashMap<Binds, Integer>();
        keyBindings.put(Binds.PLAYER_MOVE_UP, Input.KEY_Z);
        keyBindings.put(Binds.PLAYER_MOVE_LEFT, Input.KEY_Q);
        keyBindings.put(Binds.PLAYER_MOVE_DOWN, Input.KEY_S);
        keyBindings.put(Binds.PLAYER_MOVE_RIGHT, Input.KEY_D);
        keyBindings.put(Binds.PLAYER_DASH, Input.KEY_SPACE);
        keyBindings.put(Binds.PLAYER_ATTACK, Input.MOUSE_LEFT_BUTTON);
        keyBindings.put(Binds.PLAYER_DEFENCE, Input.MOUSE_RIGHT_BUTTON);

        keyStates = new HashMap<Integer, Boolean>();
        keyStates.put(Input.KEY_Z, false);
        keyStates.put(Input.KEY_Q, false);
        keyStates.put(Input.KEY_S, false);
        keyStates.put(Input.KEY_D, false);
        keyStates.put(Input.KEY_SPACE, false);
        keyStates.put(Input.MOUSE_LEFT_BUTTON, false);
        keyStates.put(Input.MOUSE_RIGHT_BUTTON, false);

        changedKeys = new ArrayList<Integer>();
        mousePosition = new Vector2f(0.f, 0.f);
    }

    public void updateKeyStates(Input input) {
        // KEYBOARD
        // Horizontal
        if(input.isKeyDown(Input.KEY_Q)) {
            toggleKeyState(Input.KEY_Q, true);
        } else {
            toggleKeyState(Input.KEY_Q, false);
        }

        if(input.isKeyDown(Input.KEY_D)) {
            toggleKeyState(Input.KEY_D, true);
        } else {
            toggleKeyState(Input.KEY_D, false);
        }

        // Vertical
        if(input.isKeyDown(Input.KEY_Z)) {
            toggleKeyState(Input.KEY_Z, true);
        } else {
            toggleKeyState(Input.KEY_Z, false);
        }

        if(input.isKeyDown(Input.KEY_S)) {
            toggleKeyState(Input.KEY_S, true);
        } else {
            toggleKeyState(Input.KEY_S, false);
        }

        if(input.isKeyPressed(Input.KEY_SPACE)) {
            toggleKeyState(Input.KEY_SPACE, true);
        } else {
            toggleKeyState(Input.KEY_SPACE, false);
        }

        // MOUSE
        if(input.isMouseButtonDown(Input.MOUSE_LEFT_BUTTON)) {
            toggleKeyState(Input.MOUSE_LEFT_BUTTON, true);
        } else {
            toggleKeyState(Input.MOUSE_LEFT_BUTTON, false);
        }
        if(input.isMouseButtonDown(Input.MOUSE_RIGHT_BUTTON)) {
            toggleKeyState(Input.MOUSE_RIGHT_BUTTON, true);
        } else {
            toggleKeyState(Input.MOUSE_RIGHT_BUTTON, false);
        }
        mousePosition.set(input.getMouseX(), input.getMouseY());
    }

    private void toggleKeyState(int key, boolean state) {
        if(keyStates.get(key) != state) {
            keyStates.put(key, state);
            changedKeys.add(key);
        }
    }

    public boolean stateChanged() {
        if(changedKeys.size() > 0) {
            return true;
        }
        return false;
    }

    public Map<Integer, Boolean> getChangedKeyStates() {
        Map<Integer, Boolean> keys = new HashMap<Integer, Boolean>();

        for(Integer key : changedKeys) {
            keys.put(key, keyStates.get(key));
        }

        return keys;
    }

    public void flushChangedKeys() {
        changedKeys.clear();
    }

    public Map<Binds, Integer> getKeyBindings() {
        return keyBindings;
    }

    public Map<Integer, Boolean> getKeyStates() {
        return keyStates;
    }

    public Vector2f getMousePosition() {
        return mousePosition;
    }
}
