package com.nael.tremendous.utils;

import org.newdawn.slick.*;
import org.newdawn.slick.Image;
import org.newdawn.slick.loading.LoadingList;
import org.newdawn.slick.util.ResourceLoader;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import java.awt.*;
import java.awt.Font;
import java.io.IOException;
import java.io.InputStream;
import java.util.HashMap;
import java.util.Map;

public class ResourceManager {

    private static ResourceManager _instance = new ResourceManager();
    private Map<String, Sound> soundMap;
    private Map<String, Image> imageMap;
    private Map<String, ResourceAnimationData> animationMap;
    private Map<String, String> textMap;
    private Map<String, TrueTypeFont> fontMap;
    private static String SPRITE_SHEET_REF = "__SPRITE_SHEET_";

    private ResourceManager() {
        soundMap 	 = new HashMap<String, Sound>();
        imageMap 	 = new HashMap<String, Image>();
        animationMap = new HashMap<String, ResourceAnimationData>();
        textMap 	 = new HashMap<String, String>();
        fontMap 	 = new HashMap<String, TrueTypeFont>();
    }

    public final static ResourceManager getInstance() {
        return _instance;
    }

    public void loadResources(InputStream is) throws SlickException {
        loadResources(is, false);
    }

    // Core method to load resources
    public void loadResources(InputStream is, boolean deferred) throws SlickException {

        DocumentBuilderFactory docBuilderFactory = DocumentBuilderFactory.newInstance();
        DocumentBuilder docBuilder = null;

        try {
            docBuilder = docBuilderFactory.newDocumentBuilder();
        } catch (ParserConfigurationException e) {
            throw new SlickException("Could not load resources", e);
        }

        Document doc = null;

        try {
            doc = docBuilder.parse(is);
        } catch (SAXException e) {
            throw new SlickException("Could not load resources", e);
        } catch (IOException e) {
            throw new SlickException("Could not load resources", e);
        }

        // normalize text representation
        doc.getDocumentElement().normalize ();

        NodeList listResources = doc.getElementsByTagName("resource");

        int totalResources = listResources.getLength();

        if(deferred) {
            LoadingList.setDeferredLoading(true);
        }

        for(int resourceIdx = 0; resourceIdx < totalResources; resourceIdx++){

            Node resourceNode = listResources.item(resourceIdx);

            if(resourceNode.getNodeType() == Node.ELEMENT_NODE){
                Element resourceElement = (Element)resourceNode;

                String type = resourceElement.getAttribute("type");

                if(type.equals("image")){
                    addElementAsImage(resourceElement);
                }else if(type.equals("sound")){
                    addElementAsSound(resourceElement);
                }else if(type.equals("text")){
                    addElementAsText(resourceElement);
                }else if(type.equals("font")){
                    addElementAsFont(resourceElement);
                }else if(type.equals("animation")){
                    addElementAsAnimation(resourceElement);
                }
            }
        }

    }


    // Image loader
    private void addElementAsImage(Element resourceElement) throws SlickException {
        loadImage(resourceElement.getAttribute("id"), resourceElement.getTextContent());
    }

    public Image loadImage(String id, String path) throws SlickException{
        if(path == null || path.length() == 0)
            throw new SlickException("Image resource [" + id + "] has invalid path");

        Image image = null;
        try{
            image = new Image(path);
        } catch (SlickException e) {
            throw new SlickException("Could not load image", e);
        }

        this.imageMap.put(id, image);
        image.setFilter(Image.FILTER_NEAREST);

        return image;
    }

    public final Image getImage(String ID){
        return imageMap.get(ID);
    }


    // Text loader
    private void addElementAsText(Element resourceElement) throws SlickException{
        loadText(resourceElement.getAttribute("id"), resourceElement.getTextContent());
    }

    public String loadText(String id, String value) throws SlickException{
        if(value == null)
            throw new SlickException("Text resource [" + id + "] has invalid value");

        textMap.put(id, value);

        return value;
    }

    public final String getText(String ID) {
        return textMap.get(ID);
    }


    // Sound loader
    private void addElementAsSound(Element resourceElement) throws SlickException {
        loadSound(resourceElement.getAttribute("id"), resourceElement.getTextContent());
    }

    public Sound loadSound(String id, String path) throws SlickException {
        if(path == null || path.length() == 0)
            throw new SlickException("Sound resource [" + id + "] has invalid path");

        Sound sound = null;

        try {
            sound = new Sound(path);
        } catch (SlickException e) {
            throw new SlickException("Could not load sound", e);
        }

        this.soundMap.put(id, sound);

        return sound;
    }

    public final Sound getSound(String ID) {
        return soundMap.get(ID);
    }


    // Fonts loader
    private void addElementAsFont(Element resourceElement) throws SlickException {
        loadFont(resourceElement.getAttribute("id"), resourceElement.getTextContent());
    }

    public TrueTypeFont loadFont(String id, String path) throws SlickException {
        if(path == null || path.length() == 0)
            throw new SlickException("Font resource [" + id + "] has invalid path");

        TrueTypeFont font = null;

        try {
            InputStream inputStream = ResourceLoader.getResourceAsStream(path);
            Font awtFont = Font.createFont(Font.TRUETYPE_FONT, inputStream);

            // TODO Add a "font size" param
            awtFont = awtFont.deriveFont(20f);
            font = new TrueTypeFont(awtFont, false);

        } catch (FontFormatException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }

        if(font == null)
            throw new SlickException("Couldn't load font");

        this.fontMap.put(id, font);

        return font;
    }

    public final TrueTypeFont getFont(String ID) {
        return fontMap.get(ID);
    }


    // Animation loader
    private void addElementAsAnimation(Element resourceElement) throws SlickException {
        loadAnimation(resourceElement.getAttribute("id"), resourceElement.getTextContent(),
                Integer.valueOf(resourceElement.getAttribute("tw")),
                Integer.valueOf(resourceElement.getAttribute("th")),
                Integer.valueOf(resourceElement.getAttribute("duration")));
    }

    private void loadAnimation(String id, String spriteSheetPath, int tw, int th, int duration) throws SlickException {

        if(spriteSheetPath == null || spriteSheetPath.length() == 0)
            throw new SlickException("Image resource [" + id + "] has invalid path");

        loadImage(SPRITE_SHEET_REF + id, spriteSheetPath);

        animationMap.put(id, new ResourceAnimationData(SPRITE_SHEET_REF + id, tw, th, duration));
    }

    public final Animation getAnimation(String ID) {
        ResourceAnimationData rad = animationMap.get(ID);

        SpriteSheet spr = new SpriteSheet(getImage(rad.getImageId()), rad.getTw(), rad.getTh());
        Animation animation = new Animation(spr, rad.getDuration());

        return animation;
    }


}
