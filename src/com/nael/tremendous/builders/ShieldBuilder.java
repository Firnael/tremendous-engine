package com.nael.tremendous.builders;

import com.apollo.Entity;
import com.apollo.EntityBuilder;
import com.apollo.World;
import com.apollo.components.Transform;
import com.nael.tremendous.components.BodyComponent;
import com.nael.tremendous.components.ShieldComponent;
import com.nael.tremendous.spatials.ShieldSpatial;

public class ShieldBuilder implements EntityBuilder {

    @Override
    public Entity buildEntity(final World world) {

        final Entity e = new Entity(world);
        e.setComponent(new Transform());
        e.setComponent(new BodyComponent(null, false, false));
        e.setComponent(new ShieldComponent());
        e.setComponent(new ShieldSpatial());
        return e;
    }
}
