package com.nael.tremendous.builders;

import com.apollo.Entity;
import com.apollo.EntityBuilder;
import com.apollo.World;
import com.apollo.components.Transform;
import com.nael.tremendous.components.AccelerationComponent;
import com.nael.tremendous.components.AttackComponent;
import com.nael.tremendous.components.BodyComponent;
import com.nael.tremendous.components.OrientationComponent;
import com.nael.tremendous.components.SpeedComponent;
import com.nael.tremendous.spatials.MobSpatial;

public class MobBuilder implements EntityBuilder {

    @Override
    public Entity buildEntity(final World world) {

        final Entity e = new Entity(world);
        e.setComponent(new Transform());
        e.setComponent(new BodyComponent(null, true, false));
        e.setComponent(new AccelerationComponent());
        e.setComponent(new SpeedComponent());
        e.setComponent(new OrientationComponent());
        e.setComponent(new AttackComponent());
        e.setComponent(new MobSpatial());
        return e;
    }
}
