package com.nael.tremendous.builders;

import com.apollo.Entity;
import com.apollo.EntityBuilder;
import com.apollo.World;
import com.apollo.components.Transform;
import com.nael.tremendous.components.AccelerationComponent;
import com.nael.tremendous.components.BodyComponent;
import com.nael.tremendous.components.InputComponent;
import com.nael.tremendous.components.OrientationComponent;
import com.nael.tremendous.components.SpeedComponent;
import com.nael.tremendous.components.WeaponComponent;
import com.nael.tremendous.spatials.PlayerSpatial;
import com.nael.tremendous.spatials.WeaponSpatial;

public class WeaponBuilder implements EntityBuilder {

    @Override
    public Entity buildEntity(final World world) {

        final Entity e = new Entity(world);
        e.setComponent(new Transform());
        e.setComponent(new BodyComponent(null, false, false));
        e.setComponent(new WeaponComponent());
        e.setComponent(new WeaponSpatial());
        return e;
    }
}
