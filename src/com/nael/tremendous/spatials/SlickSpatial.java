package com.nael.tremendous.spatials;

import com.apollo.components.spatial.Spatial;
import org.newdawn.slick.Graphics;

public abstract class SlickSpatial extends Spatial<Graphics> {

    public abstract void render(Graphics g);
}
