package com.nael.tremendous.spatials;

import com.apollo.Layer;
import com.apollo.annotate.InjectComponent;
import com.apollo.components.Transform;
import com.nael.tremendous.components.OrientationComponent;
import org.newdawn.slick.Color;
import org.newdawn.slick.Graphics;
import org.newdawn.slick.geom.Line;
import org.newdawn.slick.geom.Rectangle;
import org.newdawn.slick.geom.Vector2f;

public class MobSpatial extends SlickSpatial {

    @InjectComponent
    Transform tc;

    @InjectComponent
    OrientationComponent oc;

    private Rectangle rect;
    private Line line;

    public MobSpatial() {
        this.rect = null;
    }

    @Override
    public void initialize() {
        System.out.println("Initialising MobSpatial");
        rect = new Rectangle(tc.getX(), tc.getY(), 16, 16);
        line = getLineFromOrientation();
    }

    @Override
    public void update(int delta) {
        rect.setLocation(tc.getX(), tc.getY());
        line = getLineFromOrientation();
    }

    @Override
    public void render(Graphics g) {
        g.setColor(Color.red);
        g.draw(rect);
        g.draw(line);
        g.setColor(Color.white);
    }

    @Override
    public Layer getLayer() {
        return Layers.Actors;
    }

    private Line getLineFromOrientation() {
        Vector2f one = new Vector2f(rect.getCenterX(), rect.getCenterY());
        Vector2f two = new Vector2f(rect.getCenterX(), rect.getCenterY());

        switch(oc.getOrientation()) {
            case N: two.y = rect.getCenterY() - rect.getHeight()/2; break;
            case S: two.y = rect.getCenterY() + rect.getHeight()/2; break;
            case E: two.x = rect.getCenterX() + rect.getWidth()/2; break;
            case W: two.x = rect.getCenterX() - rect.getWidth()/2; break;
        }

        return new Line(one.x, one.y, two.x, two.y);
    }
}
