package com.nael.tremendous.spatials;

import com.apollo.Layer;
import com.apollo.annotate.InjectComponent;
import com.apollo.components.Transform;
import com.nael.tremendous.components.BodyComponent;
import com.nael.tremendous.components.OrientationComponent;
import org.newdawn.slick.Color;
import org.newdawn.slick.Graphics;
import org.newdawn.slick.geom.Line;
import org.newdawn.slick.geom.Rectangle;
import org.newdawn.slick.geom.Shape;
import org.newdawn.slick.geom.Vector2f;

public class WeaponSpatial extends SlickSpatial {

    @InjectComponent
    Transform tc;

    @InjectComponent
    BodyComponent bc;

    private Shape rect;

    public WeaponSpatial() {
        this.rect = null;
    }

    @Override
    public void initialize() {
        System.out.println("Initialising WeaponSpatial");
        rect = new Rectangle(tc.getX(), tc.getY(), bc.getBox().getWidth(), bc.getBox().getHeight());
    }

    @Override
    public void update(int delta) {
        rect = bc.getBox();
    }

    @Override
    public void render(Graphics g) {
        g.setColor(Color.yellow);
        g.draw(rect);
        g.setColor(Color.white);
    }

    @Override
    public Layer getLayer() {
        return Layers.Actors;
    }
}
