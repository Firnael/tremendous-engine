package com.nael.tremendous.spatials;

import com.apollo.Layer;
import com.apollo.annotate.InjectComponent;
import com.apollo.components.Transform;
import com.nael.tremendous.TileType;
import com.nael.tremendous.utils.ResourceManager;
import org.newdawn.slick.Graphics;
import org.newdawn.slick.Image;

public class TerrainSpatial extends SlickSpatial {

    @InjectComponent
    Transform tc;

    private Image img;
    private TileType tileType;

    public TerrainSpatial(TileType type) {
        tileType = type;

        switch(type) {
            case WALL: this.img = ResourceManager.getInstance().getImage("TERRAIN_WALL"); break;
            case FLOOR: this.img = ResourceManager.getInstance().getImage("TERRAIN_FLOOR"); break;
        }
    }

    @Override
    public void initialize() {
    }

    @Override
    public void render(Graphics g) {
        img.draw(tc.getX(), tc.getY());
    }

    public TileType getTileType() {
        return tileType;
    }

    @Override
    public Layer getLayer() {
        return Layers.Terrain;
    }
}
