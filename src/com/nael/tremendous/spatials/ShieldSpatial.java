package com.nael.tremendous.spatials;

import com.apollo.Layer;
import com.apollo.annotate.InjectComponent;
import com.apollo.components.Transform;
import com.nael.tremendous.components.BodyComponent;
import org.newdawn.slick.Color;
import org.newdawn.slick.Graphics;
import org.newdawn.slick.geom.Rectangle;
import org.newdawn.slick.geom.Shape;

public class ShieldSpatial extends SlickSpatial {

    @InjectComponent
    Transform tc;

    @InjectComponent
    BodyComponent bc;

    private Shape rect;

    public ShieldSpatial() {
        this.rect = null;
    }

    @Override
    public void initialize() {
        System.out.println("Initialising ShieldSpatial");
        rect = new Rectangle(tc.getX(), tc.getY(), bc.getBox().getWidth(), bc.getBox().getHeight());
    }

    @Override
    public void update(int delta) {
        rect = bc.getBox();
    }

    @Override
    public void render(Graphics g) {
        g.setColor(Color.green);
        g.draw(rect);
        g.setColor(Color.white);
    }

    @Override
    public Layer getLayer() {
        return Layers.Actors;
    }
}
