package com.nael.tremendous.spatials;

import com.apollo.Layer;

public enum Layers implements Layer {
    Terrain,
    Mechanisms,
    Actors,
    Interface;

    @Override
    public int getLayerId() {
        return ordinal();
    }
}
