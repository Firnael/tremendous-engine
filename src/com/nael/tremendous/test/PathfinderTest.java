/*
package com.nael.tremendous.test;


import com.nael.tremendous.managers.TerrainManager;
import com.nael.tremendous.utils.Pathfinder;
import org.junit.Assert;
import org.junit.Test;

public class PathfinderTest {

    @Test
    public void testCalculateValue() throws Exception {
        Assert.assertEquals(-2, Pathfinder.getInstance().calculateValue(-2,0));
        Assert.assertTrue(Pathfinder.getInstance().calculateValue(-1, 0) > -1);

        Assert.assertEquals(1,Pathfinder.getInstance().calculateValue(-1,0));
        Assert.assertEquals(2,Pathfinder.getInstance().calculateValue(-1,1));
        Assert.assertEquals(3,Pathfinder.getInstance().calculateValue(-1,2));

        Assert.assertEquals(1,Pathfinder.getInstance().calculateValue(1,1));
    }

    @Test
    public void setHeatMap() throws Exception {

        TerrainManager tm = new TerrainManager();
        Pathfinder instance = Pathfinder.getInstance();

        int width = tm.getWidth();
        int height = tm.getHeight();
        int[][] map = new int[width][height];

        for(int i=0; i< width; i++) {
            for(int j=0; j< height; j++) {
                if(i == 0 || j == 0 || i == width-1 || j == height-1) {
                    map[i][j]=-2;
                }
                // Square
                else if((i >= 8 && i < 12) && (j >= 8 && j < 12) ) {
                    map[i][j]=-2;
                }
                // Floor
                else {
                    map[i][j]=-1;
                }
            }
        }
        map[3][3] = 0;

        int count = 0;
        do{
            for(int i=0; i< width; i++) {
                for(int j=0; j< height; j++) {
                    instance.setHeatMapValues(width, height, map, count, i, j);
                }
            }
            count++;
        } while (count < 20);
        instance.printHeatMap(width, height, map);
    }
}
*/