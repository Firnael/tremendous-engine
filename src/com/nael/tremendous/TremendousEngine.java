package com.nael.tremendous;

import com.nael.tremendous.gamestates.GameplayState;
import com.nael.tremendous.gamestates.LoadingState;
import com.nael.tremendous.gamestates.NetworkInitState;
import org.newdawn.slick.AppGameContainer;
import org.newdawn.slick.GameContainer;
import org.newdawn.slick.SlickException;
import org.newdawn.slick.state.StateBasedGame;
import org.newdawn.slick.util.Log;

public class TremendousEngine extends StateBasedGame {

    public static final int WIDTH = 800;
    public static final int HEIGHT = 600;

    @Override
    public void initStatesList(GameContainer container) throws SlickException {
        this.addState(new NetworkInitState());
        this.addState(new LoadingState());
        this.addState(new GameplayState());
    }

    public TremendousEngine() {
        super("Tremendous Engine");
    }

    public static void main(String[] args) throws SlickException {
        AppGameContainer container = new AppGameContainer(new TremendousEngine());
        container.setDisplayMode(TremendousEngine.WIDTH, TremendousEngine.HEIGHT, false);
        container.setMinimumLogicUpdateInterval(20);
        container.setMaximumLogicUpdateInterval(20);
        container.setVSync(false);
        container.start();
        container.getGraphics().setAntiAlias(true);

        Log.info("TremendousEngine - Program started");
    }
}
