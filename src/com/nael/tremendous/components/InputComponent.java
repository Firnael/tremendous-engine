package com.nael.tremendous.components;

import com.apollo.Component;
import com.nael.tremendous.utils.InputManager;
import org.newdawn.slick.geom.Vector2f;

import java.util.HashMap;
import java.util.Map;

public class InputComponent extends Component {

    private Map<Integer, Boolean> keyStates;
    private Vector2f mousePosition;
    private boolean locallyControlled;

    public InputComponent() {
        super();

        Map<InputManager.Binds, Integer> kb = InputManager.getInstance().getKeyBindings();
        keyStates = new HashMap<Integer, Boolean>();
        keyStates.put(kb.get(InputManager.Binds.PLAYER_MOVE_UP), false);
        keyStates.put(kb.get(InputManager.Binds.PLAYER_MOVE_DOWN), false);
        keyStates.put(kb.get(InputManager.Binds.PLAYER_MOVE_LEFT), false);
        keyStates.put(kb.get(InputManager.Binds.PLAYER_MOVE_RIGHT), false);
        keyStates.put(kb.get(InputManager.Binds.PLAYER_DASH), false);
        keyStates.put(kb.get(InputManager.Binds.PLAYER_ATTACK), false);
        keyStates.put(kb.get(InputManager.Binds.PLAYER_DEFENCE), false);

        mousePosition = new Vector2f(0.f, 0.f);

        locallyControlled = false;
    }

    public boolean isLocallyControlled() {
        return locallyControlled;
    }

    public void setKeyStates(Map<Integer, Boolean> changedKeyStates) {
        for(Integer key : changedKeyStates.keySet()) {
            keyStates.put(key, changedKeyStates.get(key));
        }
    }

    public Vector2f getMousePosition() {
        return mousePosition;
    }

    public void setMousePosition(Vector2f newMousePos) {
        mousePosition = newMousePos;
    }

    public void setLocallyControlled(boolean locallyControlled) {
        this.locallyControlled = locallyControlled;
    }

    public Map<Integer, Boolean> getKeyStates() {
        return keyStates;
    }
}
