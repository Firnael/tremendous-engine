package com.nael.tremendous.components;

import com.apollo.Component;
import com.apollo.Entity;

public class AttackComponent extends Component {

    private boolean isAttacking;
    private boolean isCoolingDown;
    private long cooldown;
    private long cooldownTimer;
    private Entity weapon;

    public AttackComponent() {
        this.isAttacking = false;
        this.isCoolingDown = false;
        this.weapon = null;
        this.cooldown = 200;
        this.cooldownTimer = 0;
    }

    @Override
    public void update(int i) {
        super.update(i);

        if(isCoolingDown) {
            if(System.currentTimeMillis() - cooldownTimer > cooldown) {
                isCoolingDown = false;
            }
        }
    }

    public boolean isAttacking() {
        return isAttacking;
    }

    public void setAttacking(boolean value) {
        isAttacking = value;
    }

    public boolean isCoolingDown() {
        return isCoolingDown;
    }

    public void setCoolingDown(boolean value) {
        this.isCoolingDown = value;
    }

    public Entity getWeapon() {
        return weapon;
    }

    public void setWeapon(Entity newWeapon) {
        weapon = newWeapon;
    }

    public long getCooldown() {
        return cooldown;
    }

    public long getCooldownTimer() {
        return cooldownTimer;
    }

    public void setCooldownTimer(long cooldownTimer) {
        this.cooldownTimer = cooldownTimer;
    }
}
