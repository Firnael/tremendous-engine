package com.nael.tremendous.components;

import com.apollo.Component;
import com.apollo.Entity;
import org.newdawn.slick.geom.Vector2f;

public class WeaponComponent extends Component {

    private Entity handler;
    private Vector2f relativePositionLoading;
    private Vector2f relativePositionSwinging;
    private WeaponState state;
    private WeaponType type;
    private float loadingRange;
    private float loadingSpeed;
    private long waitingTime;
    private long waitingTimer;
    private float swingRange;
    private float swingSpeed;
    private float width;
    private float height;
    private Vector2f direction;

    public WeaponComponent() {
        this.handler = null;
        this.relativePositionLoading = new Vector2f(0f, 0f);
        this.relativePositionSwinging= new Vector2f(0f, 0f);
        this.state = null;
        this.type = null;
        this.loadingRange = 16f;
        this.loadingSpeed = 200f;
        this.waitingTime = 200;
        this.waitingTimer = 0;
        this.swingRange = 16f;
        this.swingSpeed = 4f;
        this.width = 40f;
        this.height = 6f;
        this.direction = new Vector2f(0f, 0f);
    }

    public Entity getHandler() {
        return handler;
    }

    public void setHandler(Entity newHandler) {
        handler = newHandler;
    }

    public Vector2f getRelativePositionLoading() {
        return relativePositionLoading;
    }

    public void setRelativePositionLoading(Vector2f newPosition) {
        relativePositionLoading = newPosition;
    }

    public Vector2f getRelativePositionSwinging() {
        return relativePositionSwinging;
    }

    public void setRelativePositionSwinging(Vector2f newPosition) {
        relativePositionSwinging = newPosition;
    }

    public float getLoadingRange() {
        return loadingRange;
    }

    public float getLoadingSpeed() {
        return loadingSpeed;
    }

    public long getWaitingTime() {
        return waitingTime;
    }

    public long getWaitingTimer() {
        return waitingTimer;
    }

    public float getSwingRange() {
        return swingRange;
    }

    public float getSwingSpeed() {
        return swingSpeed;
    }

    public void setWaitingTimer(long waitingTimer) {
        this.waitingTimer = waitingTimer;
    }

    public float getWidth() {
        return width;
    }

    public float getHeight() {
        return height;
    }

    public Vector2f getDirection() {
        return direction;
    }

    public void setDirection(Vector2f newDirection) {
        direction = newDirection;
    }

    public WeaponState getWeaponState() {
        return state;
    }

    public void setWeaponState(WeaponState newState) {
        state = newState;
    }

    public WeaponType getWeaponType() {
        return type;
    }

    public void setWeaponType(WeaponType newType) {
        type = newType;
    }

    public enum WeaponState {
        IDLE,
        LOADING,
        WAITING,
        SWINGING
    }

    public enum WeaponType {
        SPEAR,
        SWORD
    }
}
