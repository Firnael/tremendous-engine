package com.nael.tremendous.components;

import com.apollo.Component;
import com.apollo.Entity;

public class DefenceComponent extends Component {

    private boolean isDefending;
    private Entity shield;

    public DefenceComponent() {
        this.isDefending = false;
        this.shield = null;
    }

    public boolean isDefending() {
        return isDefending;
    }

    public void setDefending(boolean isDefending) {
        this.isDefending = isDefending;
    }

    public Entity getShield() {
        return shield;
    }

    public void setShield(Entity shield) {
        this.shield = shield;
    }
}
