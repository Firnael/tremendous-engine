package com.nael.tremendous.components;

import com.apollo.Component;
import com.apollo.Entity;
import org.newdawn.slick.geom.Vector2f;

public class ShieldComponent extends Component {

    private Entity handler;
    private float width;
    private float height;
    private Vector2f direction;

    public ShieldComponent() {
        this.handler = null;
        this.width = 32f;
        this.height = 6f;
        this.direction = new Vector2f(0f, 0f);
    }

    public Entity getHandler() {
        return handler;
    }

    public void setHandler(Entity newHandler) {
        handler = newHandler;
    }

    public float getWidth() {
        return width;
    }

    public float getHeight() {
        return height;
    }

    public Vector2f getDirection() {
        return direction;
    }

    public void setDirection(Vector2f newDirection) {
        direction = newDirection;
    }
}
