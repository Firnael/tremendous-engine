package com.nael.tremendous.components;

import com.apollo.Component;

public class NetworkIdComponent extends Component {

    private String id;

    public NetworkIdComponent(String id) {
        this.id = id;
    }

    public String getId() {
        return id;
    }
}
