package com.nael.tremendous.components.ai;

import com.apollo.Component;
import com.apollo.Entity;

public class ArrivalMovementComponent extends Component {

    private Entity arrival;
    private float slowRadius;
    private float stopRadius;

    public ArrivalMovementComponent(Entity arrival) {
        this.arrival = arrival;
        this.slowRadius = 80.f;
        this.stopRadius = 40.f;
    }

    public float getSlowRadius() {
        return slowRadius;
    }

    public float getStopRadius() {
        return stopRadius;
    }

    public Entity getArrival() {
        return arrival;
    }
}
