package com.nael.tremendous.components.ai;

import com.apollo.Component;
import org.newdawn.slick.geom.Circle;

public class WanderMovementComponent extends Component {

    private Circle circle;
    private float circleDistance;

    public WanderMovementComponent() {
        this.circle = null;
        this.circleDistance = 0f;
    }

    public Circle getCircle() {
        return circle;
    }

    public void setCircle(Circle circle) {
        this.circle = circle;
    }

    public float getCircleDistance() {
        return circleDistance;
    }

    public void setCircleDistance(float circleDistance) {
        this.circleDistance = circleDistance;
    }
}
