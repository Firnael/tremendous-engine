package com.nael.tremendous.components.ai;

import com.apollo.Component;
import com.apollo.Entity;

public class AvoidMovementComponent extends Component {

    private Entity toAvoid;
    private float radius;

    public AvoidMovementComponent(Entity toAvoid) {
        this.toAvoid = toAvoid;
        this.radius = 30.f;
    }

    public Entity getToAvoid() {
        return toAvoid;
    }

    public float getRadius() {
        return radius;
    }
}
