package com.nael.tremendous.components.ai;

import com.apollo.Component;
import com.apollo.Entity;

public class SeekMovementComponent extends Component {

    private Entity goal;
    private int viewRange;

    public SeekMovementComponent(Entity goal, int viewRange) {
        this.goal = goal;
        this.viewRange = viewRange;
    }

    public Entity getGoal() {
        return goal;
    }

    public int getViewRange() {
        return viewRange;
    }
}
