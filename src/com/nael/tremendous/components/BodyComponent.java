package com.nael.tremendous.components;

import com.apollo.Component;
import com.apollo.annotate.InjectComponent;
import com.apollo.components.Transform;
import org.newdawn.slick.geom.Vector2f;
import org.newdawn.slick.geom.Shape;

public class BodyComponent extends Component {

    @InjectComponent
    Transform transformComponent;

    private Shape box;
    // This body collides with other solid bodies
    private boolean solid;
    // This body can't be moved or pushed, it just stays still
    private boolean immutable;


    public BodyComponent() {
        this.box = null;
        this.solid = false;
        this.immutable = false;
    }

    public BodyComponent(Shape box, boolean solid, boolean immutable) {
        this.box = box;
        this.solid = solid;
        this.immutable = immutable;
    }

    @Override
    public void update(int delta) {
        box.setLocation(transformComponent.getX(), transformComponent.getY());
    }

    public Shape getBox() {
        return this.box;
    }

    public Vector2f getCenter() {
        return new Vector2f(box.getCenterX(), box.getCenterY());
    }

    public void setBox(Shape box) {
        this.box = box;
    }

    public boolean isSolid() {
        return solid;
    }

    public void setSolid(boolean value) {
        this.solid = value;
    }

    public boolean isImmutable() {
        return immutable;
    }

    public void setImmutable(boolean value) {
        this.immutable = value;
    }
}
