package com.nael.tremendous.components;

import com.apollo.Component;

public class SpeedComponent extends Component {

    private float vx, vy;
    private float maxSpeed = 100.f;


    public SpeedComponent() {
        this.vx = 0;
        this.vy = 0;
    }

    public SpeedComponent(float vx, float vy) {
        this.vx = vx;
        this.vy = vy;
    }

    public void applyAccelerationX(float ax) {
        vx += ax;
        if(Math.abs(vx) > maxSpeed) {
            vx = maxSpeed * Math.signum(vx);
        }
    }

    public void applyAccelerationY(float ay) {
        vy += ay;
        if(Math.abs(vy) > maxSpeed) {
            vy = maxSpeed * Math.signum(vy);
        }
    }

    public void setVectors(float newVx, float newVy) {
        vx = newVx;
        vy = newVy;
    }

    public float getVx() {
        return vx;
    }

    public void setVx(float newVx) {
        vx = newVx;
    }

    public float getVy() {
        return vy;
    }

    public void setVy(float newVy) {
        vy = newVy;
    }

    public float getMaxSpeed() {
        return maxSpeed;
    }

    public void setMaxSpeed(float newMaxSpeed) {
        maxSpeed = newMaxSpeed;
    }

    public boolean isMoving() {
        if(vx == 0.f && vy == 0.f) {
            return false;
        }

        return true;
    }
}
