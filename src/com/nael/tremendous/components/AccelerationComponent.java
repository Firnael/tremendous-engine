package com.nael.tremendous.components;

import com.apollo.Component;
import org.newdawn.slick.geom.Vector2f;

public class AccelerationComponent extends Component {

    private float ax;
    private float ay;
    private float a;


    public AccelerationComponent() {
        this.ax = 0;
        this.ay = 0;
        this.a = 0;
    }

    public AccelerationComponent(float initAx, float initAy) {
        this.ax = initAx;
        this.ay = initAy;
    }

    public boolean isAccelerating() {
        if(ax == 0 && ay == 0) {
            return false;
        }
        return true;
    }

    public void setVector(Vector2f vector) {
        ax = vector.getX();
        ay = vector.getY();
    }

    public Vector2f getVector() {
        return new Vector2f(ax, ay);
    }

    public float getAx() {
        return ax;
    }

    public void setAx(float ax) {
        this.ax = ax;
    }

    public float getAy() {
        return ay;
    }

    public void setAy(float ay) {
        this.ay = ay;
    }

    public float getA() {
        return a;
    }

    public void setA(float a) {
        this.a = a;
    }
}
