package com.nael.tremendous.gamestates;

import com.apollo.World;
import com.apollo.managers.GroupManager;
import com.apollo.managers.TagManager;
import com.nael.tremendous.GameStates;
import com.nael.tremendous.builders.MobBuilder;
import com.nael.tremendous.builders.PlayerBuilder;
import com.nael.tremendous.builders.ShieldBuilder;
import com.nael.tremendous.builders.WeaponBuilder;
import com.nael.tremendous.managers.*;
import com.nael.tremendous.utils.ResourceManager;
import com.nael.tremendous.utils.WorldManager;
import com.nael.tremendous.utils.network.NetworkManager;
import com.nael.tremendous.utils.network.ServerThread;
import org.newdawn.slick.GameContainer;
import org.newdawn.slick.Graphics;
import org.newdawn.slick.SlickException;
import org.newdawn.slick.state.BasicGameState;
import org.newdawn.slick.state.StateBasedGame;
import org.newdawn.slick.util.Log;

import java.io.FileInputStream;
import java.io.FileNotFoundException;

public class LoadingState extends BasicGameState {

    private StateBasedGame game;
    private GameContainer container;
    private boolean localLoading;
    private boolean networkLoading;


    @Override
    public void init(GameContainer container, StateBasedGame game) throws SlickException {
        this.game = game;
        this.container = container;
        this.localLoading = true;
    }

    @Override
    public void update(GameContainer container, StateBasedGame game, int delta) throws SlickException {
        if(localLoading) {
            // Load resources
            Log.info("[GAME] - Resources loading");
            loadResources();
            Log.info("[GAME] - Resources loaded");

            // DEBUG : Set another default font
            container.setDefaultFont(ResourceManager.getInstance().getFont("DEBUG_FONT"));

            // Create World, managers and entity builders
            Log.info("[GAME] - World creation");
            createWorld();
            Log.info("[GAME] - World created");

            Log.info("[GAME] - Game objects creation");
            createGameObjects();

            localLoading = false;
        }

        if(gameObjectAreReady()) {
            Log.info("[GAME] - Game objects created");

            // Launch server heartbeat thread
            if(NetworkManager.getInstance().isServer()) {
                ServerThread heartBeatThread = new ServerThread();
                heartBeatThread.start();
            } else {
                NetworkManager.getInstance().setReadyForHeartBeat(true);
            }

            game.enterState(GameStates.GAMEPLAY.ordinal());
        }
    }

    @Override
    public void render(GameContainer container, StateBasedGame game, Graphics g) throws SlickException {
        g.drawString("Loading...", 10, 20);
    }

    private void loadResources() {
        try {
            ResourceManager.getInstance().loadResources(new FileInputStream("data/xml/resources.xml"), true);
        } catch (FileNotFoundException e) {
            Log.error("Resource-loader XML file not found");
            System.exit(1);
        } catch (SlickException e) {
            e.printStackTrace();
            System.exit(1);
        }
    }

    private void createWorld() {
        World world = WorldManager.getInstance().getWorld();

        world.setManager(new TagManager());
        world.setManager(new GroupManager());
//        world.setManager(new BoundaryManager());
        world.setManager(new AccelerationManager());
        world.setManager(new CollisionManager());
        world.setManager(new MovementManager());
        world.setManager(new OrientationManager());
        world.setManager(new TerrainManager());
        world.setManager(new PlayerManager());
        world.setManager(new MobManager());
        world.setManager(new WeaponManager());
        world.setManager(new ShieldManager());
        world.setManager(new CameraManager(container));
        world.setManager(new CameraRenderManager(container.getGraphics()));
        world.setManager(new InputBasedMovementManager(container));
        world.setManager(new IABasedMovementManager(container));

        world.setEntityBuilder("Player", new PlayerBuilder());
        world.setEntityBuilder("Mob", new MobBuilder());
        world.setEntityBuilder("Weapon", new WeaponBuilder());
        world.setEntityBuilder("Shield", new ShieldBuilder());
    }

    private void createGameObjects() {
        World world = WorldManager.getInstance().getWorld();
        PlayerManager pm = world.getManager(PlayerManager.class);
        pm.createEntities();

        MobManager mm = world.getManager(MobManager.class);
        mm.createEntities();

        TerrainManager tm = world.getManager(TerrainManager.class);
        tm.createEntities();
    }

    private boolean gameObjectAreReady() {
        World world = WorldManager.getInstance().getWorld();
        PlayerManager pm = world.getManager(PlayerManager.class);
        MobManager mm = world.getManager(MobManager.class);
        TerrainManager tm = world.getManager(TerrainManager.class);

        if(pm.initiated && mm.initiated && tm.initiated) {
            return true;
        }
        return false;
    }

    @Override
    public int getID() {
        return GameStates.LOADING.ordinal();
    }

    public GameStates getStateName() {
        return GameStates.LOADING;
    }

    @Override
    public void enter(GameContainer container, StateBasedGame stateBasedGame) throws SlickException {
        Log.info("[STATE] - Entering state " + getStateName());
    }

    @Override
    public void leave(GameContainer container, StateBasedGame stateBasedGame) throws SlickException {
        Log.info("[STATE] - Leaving state " + getStateName());
    }
}
