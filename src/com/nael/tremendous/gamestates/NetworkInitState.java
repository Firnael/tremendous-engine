package com.nael.tremendous.gamestates;

import com.nael.tremendous.GameStates;
import com.nael.tremendous.utils.network.NetworkManager;
import com.nael.tremendous.utils.network.ServerThread;
import org.newdawn.slick.GameContainer;
import org.newdawn.slick.Graphics;
import org.newdawn.slick.SlickException;
import org.newdawn.slick.state.BasicGameState;
import org.newdawn.slick.state.StateBasedGame;
import org.newdawn.slick.util.Log;

public class NetworkInitState  extends BasicGameState {

    private StateBasedGame game;
    private GameContainer container;

    @Override
    public void init(GameContainer container, StateBasedGame game) throws SlickException {
        this.game = game;
        this.container = container;
    }

    @Override
    public void update(GameContainer container, StateBasedGame game, int delta) throws SlickException {
        // Init server
        if(NetworkManager.getInstance().isServer()) {
           NetworkManager.getInstance().initServer();
        }

        // Init client
        NetworkManager.getInstance().initClient();

        // Load game
        game.enterState(GameStates.LOADING.ordinal());
    }

    @Override
    public void render(GameContainer container, StateBasedGame game, Graphics g) throws SlickException {
        // Do nothing
    }

    @Override
    public int getID() {
        return GameStates.NETWORK_INIT.ordinal();
    }

    public GameStates getStateName() {
        return GameStates.NETWORK_INIT;
    }

    @Override
    public void enter(GameContainer container, StateBasedGame stateBasedGame) throws SlickException {
        Log.info("[STATE] - Entering state " + getStateName());
    }

    @Override
    public void leave(GameContainer container, StateBasedGame stateBasedGame) throws SlickException {
        Log.info("[STATE] - Leaving state " + getStateName());
    }
}
