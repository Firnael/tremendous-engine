package com.nael.tremendous.gamestates;

import com.apollo.World;
import com.nael.tremendous.GameStates;
import com.nael.tremendous.components.AccelerationComponent;
import com.nael.tremendous.managers.*;
import com.nael.tremendous.utils.Pathfinder;
import com.nael.tremendous.utils.WorldManager;
import org.newdawn.slick.GameContainer;
import org.newdawn.slick.Graphics;
import org.newdawn.slick.SlickException;
import org.newdawn.slick.state.BasicGameState;
import org.newdawn.slick.state.StateBasedGame;
import org.newdawn.slick.util.Log;

public class GameplayState extends BasicGameState {

    private StateBasedGame game;
    private GameContainer container;
    private World world;


    @Override
    public void init(GameContainer container, StateBasedGame game) throws SlickException {
        this.game = game;
        this.container = container;
        this.world = WorldManager.getInstance().getWorld();
    }

    @Override
    public void update(GameContainer container, StateBasedGame game, int delta) throws SlickException {
        world.update(delta);

        // Process inputs
        world.getManager(InputBasedMovementManager.class).processInputs();

        // Process entities solid collisions which update their acceleration
        world.getManager(CollisionManager.class).processSolidCollisions();

        // Process entities acceleration which update their speed
        world.getManager(AccelerationManager.class).processAccelerations(delta);

        // Process entities immutable collisions by predicting their next position regarding their current speed
        world.getManager(CollisionManager.class).processImmutableCollisions();

        // Process entities movements depending on the predicted & processed collisions
        world.getManager(MovementManager.class).processMovements();
    }

    @Override
    public void render(GameContainer container, StateBasedGame game, Graphics g) throws SlickException {
        world.getManager(CameraRenderManager.class).render(g);
    }

    @Override
    public int getID() {
        return GameStates.GAMEPLAY.ordinal();
    }

    public GameStates getStateName() {
        return GameStates.GAMEPLAY;
    }

    @Override
    public void enter(GameContainer container, StateBasedGame stateBasedGame) throws SlickException {
        Log.info("[STATE] - Entering state " + getStateName());
    }

    @Override
    public void leave(GameContainer container, StateBasedGame stateBasedGame) throws SlickException {
        Log.info("[STATE] - Entering state " + getStateName());
    }
}
