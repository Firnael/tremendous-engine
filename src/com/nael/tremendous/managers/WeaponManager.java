package com.nael.tremendous.managers;

import com.apollo.Entity;
import com.apollo.components.Transform;
import com.apollo.managers.Manager;
import com.apollo.utils.Bag;
import com.nael.tremendous.components.AttackComponent;
import com.nael.tremendous.components.BodyComponent;
import com.nael.tremendous.components.WeaponComponent;
import org.newdawn.slick.geom.Rectangle;
import org.newdawn.slick.geom.Shape;
import org.newdawn.slick.geom.Vector2f;
import org.newdawn.slick.util.Log;

public class WeaponManager extends Manager {

    @Override
    public void initialize() {
        Log.info("[GAME] - Initializing WeaponManager");
    }

    @Override
    public void update(int delta) {
        Bag<Entity> weaponEntities = world.getEntityManager().getEntitiesByComponentType(WeaponComponent.class);

        for(Entity e : weaponEntities) {
            switch(e.getComponent(WeaponComponent.class).getWeaponType()) {
                case SPEAR: updateSpear(e, delta); break;
                case SWORD: updateSword(e, delta); break;
            }
        }
    }

    private void updateSpear(Entity e, int delta) {
        WeaponComponent wc = e.getComponent(WeaponComponent.class);
        Entity handler = wc.getHandler();

        switch(wc.getWeaponState()) {
            case LOADING: {
                float distance = (delta * wc.getLoadingRange()) / wc.getLoadingSpeed();
                float newPosX = wc.getRelativePositionLoading().x - (distance * wc.getDirection().getX());
                float newPosY = wc.getRelativePositionLoading().y - (distance * wc.getDirection().getY());

                if(Math.abs(newPosX) > wc.getLoadingRange() || Math.abs(newPosY) > wc.getLoadingRange()) {
                    wc.setWaitingTimer(System.currentTimeMillis());
                    wc.setWeaponState(WeaponComponent.WeaponState.WAITING);
                }

                wc.setRelativePositionLoading(new Vector2f(newPosX, newPosY));
                updateWeaponTransformAndBody(e);
                Transform wtc = e.getComponent(Transform.class);
                wtc.setLocation(wtc.getX() + wc.getRelativePositionLoading().getX(), wtc.getY() + wc.getRelativePositionLoading().getY());
                break;
            }

            case WAITING: {
                updateWeaponTransformAndBody(e);
                Transform wtc = e.getComponent(Transform.class);
                wtc.setLocation(wtc.getX() + wc.getRelativePositionLoading().getX(), wtc.getY() + wc.getRelativePositionLoading().getY());

                if(System.currentTimeMillis() - wc.getWaitingTimer() > wc.getWaitingTime()) {
                    wc.setWeaponState(WeaponComponent.WeaponState.SWINGING);
                }
                break;
            }

            case SWINGING: {
                float distance = wc.getSwingRange() / (delta * (1/wc.getSwingSpeed()));
                float newPosX = wc.getRelativePositionSwinging().x + (distance * wc.getDirection().getX());
                float newPosY = wc.getRelativePositionSwinging().y + (distance * wc.getDirection().getY());

                if(Math.abs(newPosX) + Math.abs(newPosY) > wc.getSwingRange() * 2) {
                    wc.setWeaponState(WeaponComponent.WeaponState.IDLE);
                }

                wc.setRelativePositionSwinging(new Vector2f(newPosX, newPosY));
                updateWeaponTransformAndBody(e);
                Transform wtc = e.getComponent(Transform.class);

                float x = wtc.getX() + wc.getRelativePositionLoading().getX() + wc.getRelativePositionSwinging().getX();
                float y = wtc.getY() + wc.getRelativePositionLoading().getY() + wc.getRelativePositionSwinging().getY();
                wtc.setLocation(x, y);
                break;
            }

            case IDLE: {
                AttackComponent atc = handler.getComponent(AttackComponent.class);
                atc.setAttacking(false);
                atc.setCoolingDown(true);
                atc.setCooldownTimer(System.currentTimeMillis());
                world.deleteEntity(e);
                break;
            }
        }
    }

    private void updateSword(Entity e, int delta) {
        WeaponComponent wc = e.getComponent(WeaponComponent.class);
        Entity handler = wc.getHandler();

        switch(wc.getWeaponState()) {
            case SWINGING: {
                float distance = wc.getSwingRange() / (delta * (1/wc.getSwingSpeed()));
                float newPosX = wc.getRelativePositionSwinging().x + (distance * wc.getDirection().getX());
                float newPosY = wc.getRelativePositionSwinging().y + (distance * wc.getDirection().getY());

                if(Math.abs(newPosX) + Math.abs(newPosY) > wc.getSwingRange() * 2) {
                    wc.setWeaponState(WeaponComponent.WeaponState.IDLE);
                }

                wc.setRelativePositionSwinging(new Vector2f(newPosX, newPosY));
                updateWeaponTransformAndBody(e);
                Transform wtc = e.getComponent(Transform.class);

                float x = wtc.getX() + wc.getRelativePositionLoading().getX() + wc.getRelativePositionSwinging().getX();
                float y = wtc.getY() + wc.getRelativePositionLoading().getY() + wc.getRelativePositionSwinging().getY();
                wtc.setLocation(x, y);
                break;
            }

            case IDLE: {
                AttackComponent atc = handler.getComponent(AttackComponent.class);
                atc.setAttacking(false);
                atc.setCoolingDown(true);
                atc.setCooldownTimer(System.currentTimeMillis());
                world.deleteEntity(e);
                break;
            }
        }
    }

    public Entity addWeapon(WeaponComponent.WeaponType type, Entity handler, Vector2f direction) {
        Entity weapon = world.createEntity("Weapon");
        WeaponComponent wc = weapon.getComponent(WeaponComponent.class);
        wc.setHandler(handler);
        wc.setDirection(direction);
        wc.setWeaponType(type);

        switch(type) {
            case SPEAR: wc.setWeaponState(WeaponComponent.WeaponState.LOADING); break;
            case SWORD: wc.setWeaponState(WeaponComponent.WeaponState.SWINGING); break;
        }

        updateWeaponTransformAndBody(weapon);

        world.addEntity(weapon);
        return weapon;
    }

    private org.newdawn.slick.geom.Transform getRotationFromDirectionAndCenter(Vector2f direction, Vector2f center, float offsetAngle) {
        double angle = Math.toDegrees(Math.atan2(direction.getY(), direction.getX())) + offsetAngle;
        return org.newdawn.slick.geom.Transform.createRotateTransform((float)(angle * Math.PI) / 180f, center.getX(), center.getY());
    }

    private void updateWeaponTransformAndBody(Entity weapon) {
        WeaponComponent wc = weapon.getComponent(WeaponComponent.class);
        Entity handler = wc.getHandler();
        Vector2f direction = wc.getDirection();

        BodyComponent hbc = handler.getComponent(BodyComponent.class);
        float x = hbc.getBox().getX();
        float y = hbc.getBox().getY();
        Shape box = new Rectangle(x, y, wc.getWidth(), wc.getHeight());
        Vector2f center = new Vector2f(hbc.getCenter().getX(), hbc.getCenter().getY());

        if(wc.getWeaponType().equals(WeaponComponent.WeaponType.SPEAR)) {
            box = box.transform(getRotationFromDirectionAndCenter(direction, center, 0f));
        }
        else if(wc.getWeaponType().equals(WeaponComponent.WeaponType.SWORD)) {
            box = box.transform(getRotationFromDirectionAndCenter(direction, center, 90f));
        }

        Transform wtc = weapon.getComponent(Transform.class);
        wtc.setLocation(box.getX(), box.getY());
        weapon.getComponent(BodyComponent.class).setBox(box);
    }
}
