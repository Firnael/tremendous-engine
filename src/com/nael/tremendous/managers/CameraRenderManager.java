package com.nael.tremendous.managers;

import com.apollo.Entity;
import com.apollo.annotate.InjectManager;
import com.apollo.managers.RenderManager;
import com.nael.tremendous.components.BodyComponent;
import com.nael.tremendous.components.NetworkIdComponent;
import com.nael.tremendous.components.ai.WanderMovementComponent;
import com.nael.tremendous.utils.InputManager;
import com.nael.tremendous.utils.Pathfinder;
import com.nael.tremendous.utils.network.NetworkManager;
import org.newdawn.slick.Color;
import org.newdawn.slick.Graphics;
import org.newdawn.slick.geom.Rectangle;
import org.newdawn.slick.geom.Shape;
import org.newdawn.slick.util.Log;

public class CameraRenderManager extends RenderManager<Graphics> {

    @InjectManager
    CameraManager cameraManager;


    public CameraRenderManager(Graphics graphics) {
        super(graphics);
    }

    @Override
    public void initialize() {
        Log.info("[GAME] - Initializing CameraRenderManager");
    }

    @Override
    public void render(Graphics graphics) {
        if(cameraManager != null) {

            graphics.translate(-cameraManager.getStartX(), -cameraManager.getStartY());
            super.render(graphics);

//            renderEntitiesNetworkId(graphics);
//            renderHitBoxes(graphics);
//            renderPathfinderLines(graphics);
//            renderRotationExample(graphics);
//            renderShieldExample(graphics);
            renderWanderingCircles(graphics);

            graphics.resetTransform();

            graphics.setColor(Color.white);
            // Active entities
            graphics.drawString("Active entities: " + world.getEntityManager().getEntities().size(), 10, 25);
            // Network agent type
            renderNetworkAgentType(graphics);
            // Latendcy
            renderNetworkLatency(graphics);
        }
        else {
            Log.warn("[GAME] - CameraRenderManager : CameraManager is null");
        }
    }

    // ===

    private void renderEntitiesNetworkId(Graphics graphics) {
        for(Entity e : world.getEntityManager().getEntitiesByComponentType(NetworkIdComponent.class)) {
            NetworkIdComponent nic = e.getComponent(NetworkIdComponent.class);
            BodyComponent bc = e.getComponent(BodyComponent.class);

            graphics.drawString(nic.getId(), bc.getBox().getX(), bc.getBox().getY() - 20);
        }
    }

    private void renderHitBoxes(Graphics graphics) {
        for(Entity e : world.getEntityManager().getEntitiesByComponentType(BodyComponent.class)) {
            BodyComponent bc = e.getComponent(BodyComponent.class);
            if(bc.isImmutable()) {
                graphics.setColor(Color.yellow);
            }
            else if(bc.isSolid()) {
                graphics.setColor(Color.green);
            }
            else {
                graphics.setColor(Color.blue);
            }
            graphics.draw(bc.getBox());
        }
    }

    private void renderPathfinderLines(Graphics graphics) {
        graphics.setColor(Color.white);
        if(Pathfinder.getInstance().getVisualFeedback() != null) {
            for(Shape shape : Pathfinder.getInstance().getVisualFeedback()) {
                graphics.draw(shape);
            }
        }
    }

    private void renderNetworkAgentType(Graphics graphics) {
        graphics.setColor(Color.white);
        String agentType = "CLIENT";

        if(NetworkManager.getInstance().isServer()) {
            agentType = "SERVER";
        }

        graphics.drawString(agentType, 10, 40);
    }

    private void renderNetworkLatency(Graphics graphics) {
        graphics.setColor(Color.white);
        float latency = NetworkManager.getInstance().getLatency();
        graphics.drawString("Ping: " + latency, 10, 55);
    }

    private void renderRotationExample(Graphics graphics) {
        graphics.setColor(Color.magenta);
        Shape rect = new Rectangle(-20, -20, 40, 10);
        graphics.draw(rect);
        graphics.setColor(Color.cyan);
        rect = rect.transform(org.newdawn.slick.geom.Transform.createRotateTransform((90f * (float)Math.PI) / 180f, -20f, -20f)); // 90°
        graphics.draw(rect);
        graphics.setColor(Color.red);
        rect = new Rectangle(-20, -20, 40, 10);
        rect = rect.transform(org.newdawn.slick.geom.Transform.createRotateTransform((float)Math.PI, -20f, -20f)); // 180°
        graphics.draw(rect);
        graphics.setColor(Color.orange);
        rect = new Rectangle(-20, -20, 40, 10);
        rect = rect.transform(org.newdawn.slick.geom.Transform.createRotateTransform((-90f * (float)Math.PI) / 180f, -20f, -20f)); // -90°
        graphics.draw(rect);

        rect = new Rectangle(-20, -20, 40, 10);
        float y = InputManager.getInstance().getMousePosition().getY() +20;
        float x = InputManager.getInstance().getMousePosition().getX() +20;
        double angleToTurn = Math.toDegrees(Math.atan2(y, x));
        rect = rect.transform(org.newdawn.slick.geom.Transform.createRotateTransform((float)(angleToTurn * Math.PI / 180f), -20f, -20f));
        graphics.draw(rect);
    }

    private void renderShieldExample(Graphics graphics) {
        graphics.setColor(Color.white);
        Shape rect = new Rectangle(40, -40, 16, 16);
        graphics.draw(rect);
        graphics.setColor(Color.orange);
        rect = new Rectangle(32, -50, 32, 6);

        float y = InputManager.getInstance().getMousePosition().getY() + 32;
        float x = InputManager.getInstance().getMousePosition().getX() - 48;
        double angleToTurn = Math.toDegrees(Math.atan2(y, x)) + 90;
        rect = rect.transform(org.newdawn.slick.geom.Transform.createRotateTransform((float)(angleToTurn * Math.PI / 180f), 48f, -32f));
        graphics.draw(rect);
    }

    private void renderWanderingCircles(Graphics graphics) {
        if(NetworkManager.getInstance().isServer()) {
            for(Entity e : world.getEntityManager().getEntitiesByComponentType(WanderMovementComponent.class)) {
                graphics.draw(e.getComponent(WanderMovementComponent.class).getCircle());
            }
        }
    }
}
