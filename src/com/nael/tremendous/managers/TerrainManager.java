package com.nael.tremendous.managers;

import com.apollo.Entity;
import com.apollo.components.Transform;
import com.apollo.managers.Manager;
import com.apollo.utils.Bag;
import com.nael.tremendous.TileType;
import com.nael.tremendous.components.BodyComponent;
import com.nael.tremendous.components.IndexComponent;
import com.nael.tremendous.components.TerrainComponent;
import com.nael.tremendous.spatials.TerrainSpatial;
import com.nael.tremendous.utils.network.Network;
import com.nael.tremendous.utils.network.NetworkManager;
import org.newdawn.slick.geom.Rectangle;
import org.newdawn.slick.util.Log;

public class TerrainManager extends Manager {

    private Bag<Entity> terrainEntities;
    private int[][] array;
    private int width;
    private int height;
    private int tileSize = 20;
    public static boolean initiated;

    @Override
    public void initialize() {
        Log.info("[GAME] - Initializing TerrainManager");
    }

    public void createEntities() {
        this.terrainEntities = new Bag<Entity>();
        this.initiated = false;

        if(NetworkManager.getInstance().isServer()) {
            generateTerrain();
        }
        else {
            Log.info("[CLIENT] - Sending GetTerrain");
            Network.GetTerrain getTerrain = new Network.GetTerrain();
            NetworkManager.getInstance().getClient().sendTCP(getTerrain);
        }
    }

    @Override
    public void update(int delta) {
    }

    private void generateTerrain() {
        int w = 20;
        int h = 20;
        int[][] terrain = new int[w][h];

        for(int i=0; i<w; i++) {
            for(int j=0; j<h; j++) {

                // Border
                if(i == 0 || j == 0 || i == w-1 || j == h-1) {
                    terrain[i][j] = TileType.WALL.ordinal();
                }
                // Line 1
                else if((i == 9) && (j >= 1 && j < 9)) {
                    terrain[i][j] = TileType.WALL.ordinal();
                }
                // Line 2
                else if((i >= 10 && i < w-1) && (j == 9)) {
                    terrain[i][j] = TileType.WALL.ordinal();
                }
                // DON'T TOUCH === Floor
                else {
                    terrain[i][j] = TileType.FLOOR.ordinal();
                }
            }
        }

        generateTerrainFromArray(terrain);
    }

    public void generateTerrainFromArray(int[][] array) {
        this.array = array;
        this.width = array.length;
        this.height = array[0].length;

        for(int i=0; i<width; i++) {
            for(int j=0; j<height; j++) {

                final Entity e = new Entity(world);
                e.setComponent(new TerrainComponent());
                e.setComponent(new IndexComponent(i, j));
                e.setComponent(new Transform(i * tileSize, j * tileSize));

                if(array[i][j] == TileType.WALL.ordinal()) {
                    e.setComponent(new BodyComponent(new Rectangle(i * tileSize, j * tileSize, tileSize, tileSize), true, true));
                    e.setComponent(new TerrainSpatial(TileType.WALL));
                }
                else if(array[i][j] == TileType.FLOOR.ordinal()) {
                    e.setComponent(new BodyComponent(new Rectangle(i * tileSize, j * tileSize, tileSize, tileSize), false, false));
                    e.setComponent(new TerrainSpatial(TileType.FLOOR));
                }

                terrainEntities.add(e);
                world.addEntity(e);
            }
        }

        Log.info("[CLIENT] - TERRAIN loaded");
        initiated = true;
    }

    public int getWidth() {
        return width;
    }

    public int getHeight() {
        return height;
    }

    public Bag<Entity> getTerrainEntities() {
        return terrainEntities;
    }

    public Bag<Entity> getImmutableTerrainEntities() {
        Bag<Entity> immutableEntities = new Bag<Entity>();

        for(Entity e : terrainEntities) {
            if(e.getComponent(BodyComponent.class).isImmutable()) {
                immutableEntities.add(e);
            }
        }
        return immutableEntities;
    }

    public Entity getEntityByIndex(int x, int y) {
        for(Entity e : terrainEntities) {
            IndexComponent ic = e.getComponent(IndexComponent.class);
            if(ic.getX() == x && ic.getY() == y) {
                return e;
            }
        }
        return null;
    }

    public int[][] getTerrainAsArray() {
        /*
        for (int i = 0; i < width; i++) {
            for (int j = 0; j < height; j++) {
                System.out.print(array[j][i]);
            }
            System.out.println("");
        }
        */

        return array;
    }

    public Entity getClosestImmutableEntity(Entity e) {
        float distance = 999.f;
        Entity closest = null;

        for(Entity ie : getImmutableTerrainEntities()) {
            BodyComponent ebc = e.getComponent(BodyComponent.class);
            BodyComponent iebc = ie.getComponent(BodyComponent.class);
            float currentDistance = ebc.getCenter().distance(iebc.getCenter());

            if(currentDistance < distance) {
                distance = currentDistance;
                closest = ie;
            }
        }

        return closest;
    }
}
