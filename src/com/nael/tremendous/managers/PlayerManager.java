package com.nael.tremendous.managers;

import com.apollo.Entity;
import com.apollo.components.Transform;
import com.apollo.managers.Manager;
import com.apollo.managers.TagManager;
import com.apollo.utils.Bag;
import com.nael.tremendous.components.*;
import com.nael.tremendous.utils.network.Network;
import com.nael.tremendous.utils.network.NetworkManager;
import org.newdawn.slick.geom.Rectangle;
import org.newdawn.slick.geom.Vector2f;
import org.newdawn.slick.util.Log;

import java.util.Iterator;

public class PlayerManager extends Manager {

    private Bag<Entity> playerEntities;
    public static boolean initiated;


    @Override
    public void initialize() {
        Log.info("[GAME] - Initializing PlayerManager");
    }

    public void createEntities() {
        playerEntities = new Bag<Entity>();
        initiated = false;

        Log.info("[CLIENT] - Sending NewPlayer");
        Network.NewPlayer newPlayer = new Network.NewPlayer();
        NetworkManager.getInstance().getClient().sendTCP(newPlayer);
    }

    @Override
    public void update(int delta) {
    }

    public Entity addPlayer(Vector2f position, boolean isMe) {
        Entity player = world.createEntity("Player");
        player.getComponent(Transform.class).setLocation(position.x, position.y);
        player.getComponent(BodyComponent.class).setBox(new Rectangle(position.x, position.y, 16, 16));
        player.getComponent(AccelerationComponent.class).setA(4.f);

        if(isMe) {
            player.getComponent(InputComponent.class).setLocallyControlled(true);
            world.getManager(CameraManager.class).setTarget(player);
            TagManager tm = world.getManager(TagManager.class);
            tm.register("Player", player);
        }

        world.addEntity(player);
        playerEntities.add(player);

        return player;
    }

    public void removePlayer(String networkId) {
        Iterator it = playerEntities.iterator();
        while (it.hasNext()) {
            Entity e = (Entity)it.next();
            if(e.getComponent(NetworkIdComponent.class).getId().equals(networkId)) {
                it.remove();
                world.deleteEntity(e);
                break;
            }
        }
    }

    public Bag<Entity> getPlayerEntities() {
        return playerEntities;
    }

    public Entity getPlayerEntityByNetworkId(String id) {
        for(Entity e : playerEntities) {
            if(e.getComponent(NetworkIdComponent.class).getId().equals(id)) {
                return e;
            }
        }
        return null;
    }
}
