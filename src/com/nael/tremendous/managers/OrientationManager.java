package com.nael.tremendous.managers;

import com.apollo.Entity;
import com.apollo.managers.Manager;
import com.apollo.utils.Bag;
import com.nael.tremendous.components.InputComponent;
import com.nael.tremendous.components.OrientationComponent;
import com.nael.tremendous.components.SpeedComponent;
import org.newdawn.slick.geom.Vector2f;
import org.newdawn.slick.util.Log;

public class OrientationManager extends Manager {

    @Override
    public void initialize() {
        Log.info("[GAME] - Initializing OrientationManager");
    }

    @Override
    public void update(int delta) {
        Bag<Entity> entities = world.getEntityManager().getEntitiesByComponentType(OrientationComponent.class);

        // Check all entities with an OrientationComponent
        for(Entity e : entities) {
            // Don't update entities controlled by inputs
            if(e.getComponent(InputComponent.class) == null) {

                SpeedComponent sc = e.getComponent(SpeedComponent.class);
                if(sc.isMoving()) {
                    OrientationComponent oc = e.getComponent(OrientationComponent.class);
                    Vector2f speedNorm = new Vector2f(sc.getVx(), sc.getVy()).normalise();
                    oc.setOrientation(getOrientationFromNorm(speedNorm));
                }
            }
        }
    }

    public OrientationComponent.Orientation getOrientationFromNorm(Vector2f norm) {
        OrientationComponent.Orientation xo = OrientationComponent.Orientation.E;
        OrientationComponent.Orientation yo = OrientationComponent.Orientation.S;

        if(norm.getX() <= 0) {
            xo = OrientationComponent.Orientation.W;
        }

        if(norm.getY() <= 0) {
            yo = OrientationComponent.Orientation.N;
        }

        if(Math.abs(norm.getX()) > Math.abs(norm.getY())) {
            return xo;
        }
        return yo;
    }
}
