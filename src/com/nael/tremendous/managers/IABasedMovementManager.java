package com.nael.tremendous.managers;

import com.apollo.Entity;
import com.apollo.managers.Manager;
import com.apollo.utils.Bag;
import com.nael.tremendous.components.AccelerationComponent;
import com.nael.tremendous.components.AttackComponent;
import com.nael.tremendous.components.BodyComponent;
import com.nael.tremendous.components.NetworkIdComponent;
import com.nael.tremendous.components.SpeedComponent;
import com.nael.tremendous.components.ai.ArrivalMovementComponent;
import com.nael.tremendous.components.ai.AvoidMovementComponent;
import com.nael.tremendous.components.ai.SeekMovementComponent;
import com.nael.tremendous.components.ai.WanderMovementComponent;
import com.nael.tremendous.utils.Pathfinder;
import com.nael.tremendous.utils.network.Network;
import com.nael.tremendous.utils.network.NetworkManager;
import org.newdawn.slick.GameContainer;
import org.newdawn.slick.geom.Circle;
import org.newdawn.slick.geom.Vector2f;
import org.newdawn.slick.util.Log;

import java.util.Random;

public class IABasedMovementManager extends Manager {

    private GameContainer container;

    public IABasedMovementManager(GameContainer gc) {
        super();
        this.container = gc;
    }

    @Override
    public void initialize() {
        Log.info("[GAME] - Initializing IABasedMovementManager");
    }

    @Override
    public void update(int delta) {
        Bag<Entity> entitiesWithGoal = world.getEntityManager().getEntitiesByComponentType(SeekMovementComponent.class);
        for(Entity e : entitiesWithGoal) {
            Entity node = Pathfinder.getInstance().getNextNode(e);

            // A path exists
            if(node != null) {
                BodyComponent bc = e.getComponent(BodyComponent.class);
                BodyComponent bcg = node.getComponent(BodyComponent.class);

                // Get distance
                float distance = bc.getCenter().distance(bcg.getCenter());

                // Get norm for direction
                float x = bcg.getBox().getCenterX() - bc.getBox().getCenterX();
                float y = bcg.getBox().getCenterY() - bc.getBox().getCenterY();
                Vector2f vector = new Vector2f(x, y).normalise();

                // Avoid walls
                Entity closestWall = world.getManager(TerrainManager.class).getClosestImmutableEntity(e);
                BodyComponent bcw = closestWall.getComponent(BodyComponent.class);

                if(bcw.getCenter().distance(bc.getCenter()) < (bcw.getBox().getBoundingCircleRadius() + bc.getBox().getBoundingCircleRadius())) {
                    float wx = bc.getBox().getCenterX() - bcw.getBox().getCenterX();
                    float wy = bc.getBox().getCenterY() - bcw.getBox().getCenterY();
                    Vector2f v = new Vector2f(-wx, -wy).normalise();

                    vector = new Vector2f(vector.getX() - v.getX(), vector.getY() - v.getY()).normalise();
                }

                // Get base acceleration
                AccelerationComponent ac = e.getComponent(AccelerationComponent.class);
                float acceleration = ac.getA();

                // Apply arrival force if needed
                if(e.getComponent(ArrivalMovementComponent.class) != null) {
                    ArrivalMovementComponent amc = e.getComponent(ArrivalMovementComponent.class);
                    Entity arrival = amc.getArrival();

                    if(arrival.equals(node)) {
                        // Alter acceleration depending on distance (arrival)
                        float slowRadius = amc.getSlowRadius();
                        float stopRadius = amc.getStopRadius();
                        if(distance < slowRadius) {
                            if(distance < stopRadius) {
                                acceleration = 0.f;
                            }
                            else {
                                float slowFactor = distance / slowRadius;
                                acceleration *= slowFactor;
                            }
                        }
                    }
                }

                ac.setAx(vector.x * acceleration);
                ac.setAy(vector.y * acceleration);
            }
        }

        // Avoid
        Bag<Entity> entitiesWithAvoid = world.getEntityManager().getEntitiesByComponentType(AvoidMovementComponent.class);
        for(Entity e : entitiesWithAvoid) {
            AvoidMovementComponent amc = e.getComponent(AvoidMovementComponent.class);
            Entity ea = amc.getToAvoid();

            BodyComponent bc = e.getComponent(BodyComponent.class);
            BodyComponent bca = ea.getComponent(BodyComponent.class);

            if(bc.getCenter().distance(bca.getCenter()) < amc.getRadius()) {
                float x = bc.getBox().getCenterX() - bca.getBox().getCenterX();
                float y = bc.getBox().getCenterY() - bca.getBox().getCenterY();
                Vector2f norm = new Vector2f(x, y).normalise();

                // Get base acceleration
                AccelerationComponent ac = e.getComponent(AccelerationComponent.class);
                float acceleration = ac.getA();

                ac.setAx(norm.x * acceleration);
                ac.setAy(norm.y * acceleration);
            }
        }

        // Wander
        Bag<Entity> entitiesWithWander = world.getEntityManager().getEntitiesByComponentType(WanderMovementComponent.class);
        for(Entity e : entitiesWithWander) {
            WanderMovementComponent wmc = e.getComponent(WanderMovementComponent.class);
            SpeedComponent sc = e.getComponent(SpeedComponent.class);

            if(NetworkManager.getInstance().isServer()) {
                float x = sc.getVx();
                float y = sc.getVy();

                if (x == 0 && y == 0) {
                    x = 1f;
                    y = 1f;
                }

                Vector2f norm = new Vector2f(x, y).normalise();

                BodyComponent bc = e.getComponent(BodyComponent.class);
                float circleDistance = ((bc.getBox().getWidth() + bc.getBox().getHeight()));

                float centerX = bc.getCenter().getX() + (circleDistance * norm.getX());
                float centerY = bc.getCenter().getY() + (circleDistance * norm.getY());
                Circle circle = new Circle(centerX, centerY, bc.getBox().getBoundingCircleRadius() * 2);
                wmc.setCircle(circle);

                // Get random point frm the circle
                float[] points = circle.getPoints();
                Random rand = new Random();
                int randPointIndex = rand.nextInt(points.length);
                if (randPointIndex % 2 != 0) {
                    randPointIndex--;
                }

                Vector2f point = new Vector2f(points[randPointIndex], points[randPointIndex + 1]);
                Vector2f newNorm = new Vector2f(point.getX() - bc.getCenter().getX(), point.getY() - bc.getCenter().getY()).normalise();

                // Get base acceleration
                AccelerationComponent ac = e.getComponent(AccelerationComponent.class);
                float acceleration = ac.getA();

                ac.setAx(newNorm.x * (acceleration * 0.5f));
                ac.setAy(newNorm.y * (acceleration * 0.5f));
            }
        }

        // Attack
        if(NetworkManager.getInstance().isServer()) {
            Bag<Entity> entitiesWithAttack = world.getManager(MobManager.class).getMobEntities();
            for(Entity e : entitiesWithAttack) {
                for(Entity pe : world.getManager(PlayerManager.class).getPlayerEntities()) {
                    BodyComponent bc = e.getComponent(BodyComponent.class);
                    BodyComponent bcp = pe.getComponent(BodyComponent.class);

                    if(bc.getCenter().distance(bcp.getCenter()) < 40) {
                        AttackComponent atc = e.getComponent(AttackComponent.class);
                        if(!atc.isAttacking() && !atc.isCoolingDown()) {
                            // Get norm
                            Vector2f norm = new Vector2f(bcp.getCenter().getX() - bc.getCenter().getX(), bcp.getCenter().getY() - bc.getCenter().getY()).normalise();

                            Log.info("[SERVER] - Sending AIAttack");
                            Network.AIAttack aiAttack = new Network.AIAttack();
                            aiAttack.networkId = e.getComponent(NetworkIdComponent.class).getId();
                            aiAttack.norm = norm;
                            NetworkManager.getInstance().getServer().sendToAllTCP(aiAttack);
                        }

                        break;
                    }
                }
            }
        }

    }
}
