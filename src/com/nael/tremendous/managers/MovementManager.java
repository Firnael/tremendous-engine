package com.nael.tremendous.managers;

import com.apollo.Entity;
import com.apollo.components.Transform;
import com.apollo.managers.EntityManager;
import com.apollo.managers.Manager;
import com.apollo.utils.Bag;
import com.nael.tremendous.components.AccelerationComponent;
import com.nael.tremendous.components.NetworkIdComponent;
import com.nael.tremendous.components.SpeedComponent;
import org.newdawn.slick.util.Log;

public class MovementManager extends Manager {

    private Bag<Entity> dynamicsEntities;
    private float frictionConstant;
    private float minimalSpeedValue;


    @Override
    public void initialize() {
        Log.info("[GAME] - Initializing MovementManager");
        frictionConstant = 0.2f;
        minimalSpeedValue = 0.01f;
    }

    @Override
    public void update(int delta) {
        EntityManager em = world.getEntityManager();
        dynamicsEntities = em.getEntitiesByComponentType(SpeedComponent.class);

        for(Entity e : dynamicsEntities) {
            SpeedComponent sc = e.getComponent(SpeedComponent.class);

            // Apply frictions
            if(sc.isMoving()) {

                // Adjust vector norm if the entity is moving in 2D
                double vn = Math.sqrt((sc.getVx() * sc.getVx()) + (sc.getVy() * sc.getVy()));

                if(vn > sc.getMaxSpeed()) {

                    boolean isNegative = false;
                    if(sc.getVx() < 0) {
                        sc.setVx(sc.getVx() * -1);
                        isNegative = true;
                    }

                    double theta = Math.atan(sc.getVy() / sc.getVx());

                    float newVx = sc.getMaxSpeed() * (float)Math.cos(theta);
                    float newVy = sc.getMaxSpeed() * (float)Math.sin(theta);

                    if(isNegative) {
                        newVx *= -1;
                    }

                    sc.setVx(newVx);
                    sc.setVy(newVy);
                }

                // Whatever happens, speed always goes down
                sc.setVx(sc.getVx() - (frictionConstant * sc.getVx()));
                sc.setVy(sc.getVy() - (frictionConstant * sc.getVy()));


                // Don't compute too small values, just negate speed instead
                if(Math.abs(sc.getVx()) < minimalSpeedValue) {
                    sc.setVx(0.0f);
                }

                if(Math.abs(sc.getVy()) < minimalSpeedValue) {
                    sc.setVy(0.0f);
                }
            }
        }
    }

    public void processMovements() {

        for(Entity e : dynamicsEntities) {
            Transform transform = e.getComponent(Transform.class);
            SpeedComponent speed = e.getComponent(SpeedComponent.class);

            transform.addX(speed.getVx());
            transform.addY(speed.getVy());
        }
    }
}
