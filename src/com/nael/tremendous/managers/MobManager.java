package com.nael.tremendous.managers;

import com.apollo.Entity;
import com.apollo.components.Transform;
import com.apollo.managers.Manager;
import com.apollo.utils.Bag;
import com.nael.tremendous.components.AccelerationComponent;
import com.nael.tremendous.components.BodyComponent;
import com.nael.tremendous.components.NetworkIdComponent;
import com.nael.tremendous.components.ai.ArrivalMovementComponent;
import com.nael.tremendous.components.ai.AvoidMovementComponent;
import com.nael.tremendous.components.ai.SeekMovementComponent;
import com.nael.tremendous.components.ai.WanderMovementComponent;
import com.nael.tremendous.utils.network.Network;
import com.nael.tremendous.utils.network.NetworkManager;
import org.newdawn.slick.geom.Rectangle;
import org.newdawn.slick.geom.Vector2f;
import org.newdawn.slick.util.Log;

import java.util.Iterator;

public class MobManager extends Manager {

    private Bag<Entity> mobEntities;
    private int mobCount;
    private long mobIdCount = 1;
    public static boolean initiated;

    @Override
    public void initialize() {
        Log.info("[GAME] - Initializing MobManager");
    }

    public void createEntities() {
        mobEntities = new Bag<Entity>();
        initiated = false;

        NetworkManager nm = NetworkManager.getInstance();
        if(!nm.isServer()) {
            Log.info("[CLIENT] - Sending GetMob");
            Network.GetMobs getMobs = new Network.GetMobs();
            nm.getClient().sendTCP(getMobs);
        }
        else {
            initiated = true;
        }
    }

    @Override
    public void update(int delta) {
        if(NetworkManager.getInstance().isServer()) {
            Entity player = world.getManager(PlayerManager.class).getPlayerEntityByNetworkId("Player@1");
            if(player != null && mobEntities.size() == 0) {
                Entity mob = world.getManager(MobManager.class).addMob(new Vector2f(210.f, 42.f));
                mob.setComponent(new WanderMovementComponent());
                mob.setComponent(new NetworkIdComponent(getNewMobId()));

                Entity mob2 = world.getManager(MobManager.class).addMob(new Vector2f(300.f, 360.f));
                mob2.setComponent(new SeekMovementComponent(player, 100));
                mob2.setComponent(new ArrivalMovementComponent(player));
                mob2.setComponent(new AvoidMovementComponent(player));
                mob2.setComponent(new NetworkIdComponent(getNewMobId()));
            }
        }
    }

    public Entity addMob(Vector2f position) {
        Entity mob = world.createEntity("Mob");
        mob.getComponent(Transform.class).setLocation(position.x, position.y);
        mob.getComponent(BodyComponent.class).setBox(new Rectangle(position.x, position.y, 16, 16));
        mob.getComponent(AccelerationComponent.class).setA(4.f);

        world.addEntity(mob);
        mobEntities.add(mob);

        return mob;
    }

    public void removeMob(String networkId) {
        Iterator it = mobEntities.iterator();
        while (it.hasNext()) {
            Entity e = (Entity)it.next();
            if(e.getComponent(NetworkIdComponent.class).getId().equals(networkId)) {
                it.remove();
                world.deleteEntity(e);
                break;
            }
        }
    }

    public Bag<Entity> getMobEntities() {
        return mobEntities;
    }

    public Entity getMobEntityByNetworkId(String id) {
        for(Entity e : mobEntities) {
            if(e.getComponent(NetworkIdComponent.class).getId().equals(id)) {
                return e;
            }
        }
        return null;
    }

    public void setMobCount(int mobCount) {
        this.mobCount = mobCount;
    }

    public int getMobCount() {
        return mobCount;
    }

    private String getNewMobId() {
        if(NetworkManager.getInstance().isServer()) {
            String id = "Mob@" + mobIdCount;
            mobIdCount++;
            return id;
        }
        else {
            Log.warn("[CLIENT] Shouldn't be using method 'getNewMobId'");
            return null;
        }
    }
}
