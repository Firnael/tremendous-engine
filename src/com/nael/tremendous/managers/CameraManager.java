package com.nael.tremendous.managers;

import com.apollo.Entity;
import com.apollo.annotate.InjectManager;
import com.apollo.components.Transform;
import com.apollo.managers.Manager;
import com.apollo.managers.TagManager;
import com.nael.tremendous.Tags;
import org.newdawn.slick.GameContainer;
import org.newdawn.slick.Input;
import org.newdawn.slick.util.Log;

public class CameraManager extends Manager {

    private GameContainer container;
    private float lookAtX;
    private float lookAtY;
    private int screenWidth;
    private int screenHeight;
    private Input input;
    private Entity target;

    @InjectManager
    BoundaryManager boundaryManager;


    public CameraManager(GameContainer container) {
        super();
        this.container = container;
        this.target = null;
    }

    @Override
    public void initialize() {
        Log.info("[GAME] - Initializing CameraManager");

        input = container.getInput();

        screenWidth = container.getWidth();
        screenHeight = container.getHeight();
    }

    @Override
    public void update(int delta) {
        if(target != null) {

            Transform tc = target.getComponent(Transform.class);

            if(Float.isNaN(tc.getX()) || Float.isNaN(tc.getX())) {
                target = world.getManager(TagManager.class).getEntity("Player");
            }

            lookAtX = target.getComponent(Transform.class).getX();
            lookAtY = target.getComponent(Transform.class).getY();

            //fixBoundaries();

            input.setOffset(getStartX(), getStartY());
        }
        else {
            Log.warn("[GAME] - CameraManager : My target is null");
        }
    }

    public void setTarget(Entity e) {
        target = e;
    }

    private void fixBoundaries() {

        if (getEndX() > boundaryManager.getBoundaryWidth()) {
            lookAtX -= getEndX() - boundaryManager.getBoundaryWidth();
        } else if (getStartX() < 0) {
            lookAtX -= getStartX();
        }

        if (getEndY() > boundaryManager.getBoundaryHeight()) {
            lookAtY -= getEndY() - boundaryManager.getBoundaryHeight();
        } else if (getStartY() < 0) {
            lookAtY -= getStartY();
        }
    }

    private float getOffsetX() {
        return ((float) screenWidth) / 2f;
    }

    private float getOffsetY() {
        return ((float) screenHeight) / 2f;
    }

    public float getStartX() {
        return lookAtX - getOffsetX();
    }

    public float getStartY() {
        return lookAtY - getOffsetY();
    }

    public float getEndX() {
        return lookAtX + getOffsetX();
    }

    public float getEndY() {
        return lookAtY + getOffsetY();
    }

    public float getWidth() {
        return getEndX() - getStartX();
    }

    public float getHeight() {
        return getEndY() - getStartY();
    }
}
