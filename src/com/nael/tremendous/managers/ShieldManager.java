package com.nael.tremendous.managers;

import com.apollo.Entity;
import com.apollo.components.Transform;
import com.apollo.managers.Manager;
import com.apollo.utils.Bag;
import com.nael.tremendous.components.BodyComponent;
import com.nael.tremendous.components.DefenceComponent;
import com.nael.tremendous.components.ShieldComponent;
import org.newdawn.slick.geom.Rectangle;
import org.newdawn.slick.geom.Shape;
import org.newdawn.slick.geom.Vector2f;
import org.newdawn.slick.util.Log;

public class ShieldManager extends Manager {

    @Override
    public void initialize() {
        Log.info("[GAME] - Initializing ShieldManager");
    }

    @Override
    public void update(int delta) {
        Bag<Entity> shieldEntities = world.getEntityManager().getEntitiesByComponentType(ShieldComponent.class);

        for(Entity e : shieldEntities) {
            ShieldComponent shc = e.getComponent(ShieldComponent.class);
            Entity handler = shc.getHandler();
            DefenceComponent dc = handler.getComponent(DefenceComponent.class);

            if(dc.isDefending()) {
                updateShieldTransformAndBody(e);
            }
            else {
                world.deleteEntity(dc.getShield());
            }
        }
    }

    public Entity addShield(Entity handler, Vector2f direction) {
        Entity shield = world.createEntity("Shield");
        ShieldComponent shc = shield.getComponent(ShieldComponent.class);
        shc.setHandler(handler);
        shc.setDirection(direction);

        updateShieldTransformAndBody(shield);

        world.addEntity(shield);
        return shield;
    }

    private org.newdawn.slick.geom.Transform getRotationFromDirectionAndCenter(Vector2f direction, Vector2f center) {
        double angle = Math.toDegrees(Math.atan2(direction.getY(), direction.getX())) + 90;
        return org.newdawn.slick.geom.Transform.createRotateTransform((float)(angle * Math.PI) / 180f, center.getX(), center.getY());
    }

    private void updateShieldTransformAndBody(Entity shield) {
        ShieldComponent shc = shield.getComponent(ShieldComponent.class);
        Entity handler = shc.getHandler();

        BodyComponent hbc = handler.getComponent(BodyComponent.class);
        Vector2f position = new Vector2f(hbc.getBox().getX() - (hbc.getBox().getWidth()/2) ,hbc.getBox().getY() - 10);
        Shape box = new Rectangle(position.getX(), position.getY(), shc.getWidth(), shc.getHeight());
        box = box.transform(getRotationFromDirectionAndCenter(shc.getDirection(), hbc.getCenter()));

        Transform shtc = shield.getComponent(Transform.class);
        shtc.setLocation(box.getX(), box.getY());
        shield.getComponent(BodyComponent.class).setBox(box);
    }
}
