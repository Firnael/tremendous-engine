package com.nael.tremendous.managers;

import com.apollo.Entity;
import com.apollo.managers.Manager;
import com.apollo.utils.Bag;
import com.nael.tremendous.components.AttackComponent;
import com.nael.tremendous.components.BodyComponent;
import com.nael.tremendous.components.ShieldComponent;
import com.nael.tremendous.components.SpeedComponent;
import com.nael.tremendous.components.WeaponComponent;
import org.newdawn.slick.geom.Rectangle;
import org.newdawn.slick.geom.Shape;
import org.newdawn.slick.geom.Vector2f;
import org.newdawn.slick.util.Log;

public class CollisionManager extends Manager {

    private Bag<Entity> solidEntitiesBag;
    private Bag<Entity> immutableEntitiesBag;


    @Override
    public void initialize() {
        Log.info("[GAME] - Initializing CollisionManager");
        solidEntitiesBag = new Bag<Entity>();
        immutableEntitiesBag = new Bag<Entity>();
    }

    @Override
    public void update(int delta) {
        immutableEntitiesBag.clear();
        solidEntitiesBag.clear();

        for(Entity e : world.getEntityManager().getEntitiesByComponentType(BodyComponent.class)) {
            BodyComponent bc = e.getComponent(BodyComponent.class);
            if(bc.isImmutable()) {
                immutableEntitiesBag.add(e);
            }
            else if(bc.isSolid()) {
                solidEntitiesBag.add(e);
            }
        }

        processWeaponCollisions();
    }

    public void processWeaponCollisions() {
        for(Entity weapon : world.getEntityManager().getEntitiesByComponentType(WeaponComponent.class)) {
            BodyComponent wbc = weapon.getComponent(BodyComponent.class);
            WeaponComponent wc = weapon.getComponent(WeaponComponent.class);

            if(wc.getWeaponState() == WeaponComponent.WeaponState.SWINGING) {

                // Intersects with shields
                for (Entity shield : world.getEntityManager().getEntitiesByComponentType(ShieldComponent.class)) {
                    BodyComponent shbc = shield.getComponent(BodyComponent.class);

                    if (wbc.getBox().intersects(shbc.getBox())) {
                        Entity defender = shield.getComponent(ShieldComponent.class).getHandler();
                        BodyComponent dbc = defender.getComponent(BodyComponent.class);

                        float x = dbc.getCenter().getX() - wbc.getCenter().getX();
                        float y = dbc.getCenter().getY() - wbc.getCenter().getY();
                        Vector2f norm = new Vector2f(x, y).normalise();

                        SpeedComponent dsc = defender.getComponent(SpeedComponent.class);
                        dsc.setVx(dsc.getVx() + (2 * norm.getX()));
                        dsc.setVy(dsc.getVy() + (2 * norm.getY()));

                        // Bump the weapon handler
                        Entity attacker = wc.getHandler();
                        SpeedComponent asc = attacker.getComponent(SpeedComponent.class);
                        asc.setVx(asc.getVx() - (2 * norm.getX()));
                        asc.setVy(asc.getVy() - (2 * norm.getY()));

                        // Stop the weapon swing movement
                        weapon.getComponent(WeaponComponent.class).setWeaponState(WeaponComponent.WeaponState.IDLE);
                    }
                }

                // Intersect with living things
                for (Entity attacker : world.getEntityManager().getEntitiesByComponentType(AttackComponent.class)) {
                    BodyComponent abc = attacker.getComponent(BodyComponent.class);

                    if (wc.getHandler() != attacker) {
                        if (wbc.getBox().intersects(abc.getBox())) {
                            float x = abc.getCenter().getX() - wbc.getCenter().getX();
                            float y = abc.getCenter().getY() - wbc.getCenter().getY();
                            Vector2f norm = new Vector2f(x, y).normalise();

                            SpeedComponent msc = attacker.getComponent(SpeedComponent.class);
                            msc.setVx(msc.getVx() + (4 * norm.getX()));
                            msc.setVy(msc.getVy() + (4 * norm.getY()));
                        }
                    }
                }
            }
        }
    }

    public void processSolidCollisions() {
        // SOLID COLLISIONS
        // Ex : Player1 and Player2 entities
        for(Entity e1 : solidEntitiesBag) {
            for(Entity e2 : solidEntitiesBag) {
                if(!e1.equals(e2)) {
                    if(areColliding(getEntityBox(e1), getEntityBox(e2))) {
                        BodyComponent e1Bc = e1.getComponent(BodyComponent.class);
                        BodyComponent e2Bc = e2.getComponent(BodyComponent.class);

                        Vector2f vector = new Vector2f(e2Bc.getBox().getCenterX() - e1Bc.getBox().getCenterX(),
                            e2Bc.getBox().getCenterY() - e1Bc.getBox().getCenterY()).normalise();

                        float centersDistance = e1Bc.getCenter().distance(e2Bc.getCenter());
                        if(centersDistance == 0) {
                            centersDistance = 1;
                        }

                        SpeedComponent e2Sc = e2.getComponent(SpeedComponent.class);

                        e2Sc.setVx(vector.getX() * (10/centersDistance));
                        e2Sc.setVy(vector.getY() * (10/centersDistance));
                    }
                }
            }
        }
    }

    public void processImmutableCollisions() {
        // IMMUTABLE COLLISIONS
        // Ex : Player1 and RandomWall entities
        for(Entity e : solidEntitiesBag) {

            // At first, check if the entities collide (no need to affine movement if they don't)
            boolean needToAffine = false;

            // Get the entity box and update its position to simulate its state at T+1
            Shape box = getEntityBox(e);
            Shape futureBox = new Rectangle(box.getX(), box.getY(), box.getWidth(), box.getHeight());

            SpeedComponent sc = e.getComponent(SpeedComponent.class);
            float vx = sc.getVx();
            float vy = sc.getVy();

            futureBox.setX(futureBox.getX() + vx);
            futureBox.setY(futureBox.getY() + vy);

            // Collision check with the simulated box
            for(Entity ie : immutableEntitiesBag) {
                if(areColliding(futureBox, getEntityBox(ie))) {
                    needToAffine = true;
                    break;
                }
            }

            // If there is no need to affine the vectors, jump to the next iteration
            if(!needToAffine) {
                continue;
            }
            else {
                boolean collideX = false;
                boolean collideY = false;

                for(Entity ie : immutableEntitiesBag) {
                    Shape immutableBox = getEntityBox(ie);

                    // X
                    futureBox = new Rectangle(box.getX(), box.getY(), box.getWidth(), box.getHeight());
                    futureBox.setX(futureBox.getX() + vx);

                    if(areColliding(futureBox, immutableBox)) {
                        collideX = true;
                    }

                    // Y
                    futureBox = new Rectangle(box.getX(), box.getY(), box.getWidth(), box.getHeight());
                    futureBox.setY(futureBox.getY() + vy);

                    if(areColliding(futureBox, immutableBox)) {
                        collideY = true;
                    }

                    // BOTH
                    if(collideX && collideY)
                        break;
                }

                int lowerVx = (int)Math.abs(vx);
                int lowerVy = (int)Math.abs(vy);

                for(Entity ie : immutableEntitiesBag) {
                    Shape immutableBox = getEntityBox(ie);

                    // Stop doing this if you can't go further
                    if((lowerVx == 0 && lowerVy == 0))
                        break;

                    // X
                    if(collideX) {
                        for(int i=1; i<=lowerVx; i++) {
                            futureBox = new Rectangle(box.getX(), box.getY(), box.getWidth(), box.getHeight());
                            futureBox.setX(futureBox.getX() + (i * Math.signum(vx)));

                            if(areColliding(futureBox, immutableBox)) {
                                lowerVx = i-1;
                                break;
                            }
                        }
                    }

                    // Y
                    if(collideY) {
                        for(int i=1; i<=lowerVy; i++) {
                            futureBox = new Rectangle(box.getX(), box.getY(), box.getWidth(), box.getHeight());
                            futureBox.setY(futureBox.getY() + (i * Math.signum(vy)));

                            if(areColliding(futureBox, immutableBox)) {
                                lowerVy = i-1;
                                break;
                            }
                        }
                    }
                }

                // Don't update the speed values if they are too small to adjust
                if(collideX) {
                    sc.setVx((lowerVx * Math.signum(vx)));
                }
                if(collideY) {
                    sc.setVy((lowerVy * Math.signum(vy)));
                }
            }
        }
    }

    private boolean areColliding(Entity one, Entity two) {
        return areColliding(getEntityBox(one), getEntityBox(two));
    }

    private boolean areColliding(Shape one, Shape two) {
        if(one.intersects(two)) {
            return true;
        }
        return false;
    }

    private Shape getEntityBox(Entity e) {
        return e.getComponent(BodyComponent.class).getBox();
    }
}
