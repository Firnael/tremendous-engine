package com.nael.tremendous.managers;

import com.apollo.Entity;
import com.apollo.managers.Manager;
import com.nael.tremendous.components.AccelerationComponent;
import com.nael.tremendous.components.AttackComponent;
import com.nael.tremendous.components.BodyComponent;
import com.nael.tremendous.components.DefenceComponent;
import com.nael.tremendous.components.InputComponent;
import com.nael.tremendous.components.OrientationComponent;
import com.nael.tremendous.components.ShieldComponent;
import com.nael.tremendous.components.WeaponComponent;
import com.nael.tremendous.utils.InputManager;
import com.nael.tremendous.utils.network.Network;
import com.nael.tremendous.utils.network.NetworkManager;
import org.newdawn.slick.GameContainer;
import org.newdawn.slick.Input;
import org.newdawn.slick.geom.Vector2f;
import org.newdawn.slick.util.Log;

public class InputBasedMovementManager extends Manager {

    private GameContainer container;


    public InputBasedMovementManager(GameContainer gc) {
        super();
        this.container = gc;
    }

    @Override
    public void initialize() {
        Log.info("[GAME] - Initializing InputBasedMovementManager");
    }

    @Override
    public void update(int delta) {
        Input input = container.getInput();
        InputManager im = InputManager.getInstance();
        im.updateKeyStates(input);

        // Local
        for (Entity e : world.getEntityManager().getEntitiesByComponentType(InputComponent.class)) {
            InputComponent ic = e.getComponent(InputComponent.class);
            if (ic.isLocallyControlled()) {
                ic.setKeyStates(im.getKeyStates());
            }
        }

        // Network
        if (im.stateChanged()) {
            Log.info("[CLIENT] - Sending PlayerInput");
            Network.PlayerInput playerInput = new Network.PlayerInput();
            playerInput.keyStates = im.getChangedKeyStates();
            playerInput.mousePosition = im.getMousePosition();
            NetworkManager.getInstance().getClient().sendTCP(playerInput);
        }

        // Flush changed keys state
        im.flushChangedKeys();
    }

    public void processInputs() {
        Input input = container.getInput();
        InputManager im = InputManager.getInstance();

        for (Entity e : world.getEntityManager().getEntitiesByComponentType(InputComponent.class)) {
            OrientationComponent oc = e.getComponent(OrientationComponent.class);
            OrientationComponent.Orientation moveOrientation = null;
            OrientationComponent.Orientation attackOrientation = null;
            OrientationComponent.Orientation defenceOrientation = null;
            Vector2f vector = new Vector2f(0.f, 0.f);

            // KEYBOARD
            int leftKey = im.getKeyBindings().get(InputManager.Binds.PLAYER_MOVE_LEFT);
            int rightKey = im.getKeyBindings().get(InputManager.Binds.PLAYER_MOVE_RIGHT);
            int upKey = im.getKeyBindings().get(InputManager.Binds.PLAYER_MOVE_UP);
            int downKey = im.getKeyBindings().get(InputManager.Binds.PLAYER_MOVE_DOWN);

            InputComponent ic = e.getComponent(InputComponent.class);

            // Left
            if (ic.getKeyStates().get(leftKey)) {
                vector.x = -1.f;
                moveOrientation = OrientationComponent.Orientation.W;
            }
            // Right
            if (ic.getKeyStates().get(rightKey)) {
                vector.x = 1.f;
                moveOrientation = OrientationComponent.Orientation.E;
            }
            // Up
            if (ic.getKeyStates().get(upKey)) {
                vector.y = -1.f;
                moveOrientation = OrientationComponent.Orientation.N;
            }
            // Down
            if (ic.getKeyStates().get(downKey)) {
                vector.y = 1.f;
                moveOrientation = OrientationComponent.Orientation.S;
            }

            float acceleration = e.getComponent(AccelerationComponent.class).getA();
            AccelerationComponent ac = e.getComponent(AccelerationComponent.class);
            ac.setAx(vector.x * acceleration);
            ac.setAy(vector.y * acceleration);

            // Dash
            int dashKey = im.getKeyBindings().get(InputManager.Binds.PLAYER_DASH);
            if (ic.getKeyStates().get(dashKey)) {
                Vector2f dashValue = new Vector2f(100.f * ac.getVector().normalise().getX(), 100.f * ac.getVector().normalise().getY());
                ac.setAx(ac.getAx() + dashValue.getX());
                ac.setAy(ac.getAy() + dashValue.getY());
            }


            // MOUSE
            // Get player's mouse position
            BodyComponent pbc = e.getComponent(BodyComponent.class);
            Vector2f mp = ic.getMousePosition();
            if(ic.isLocallyControlled()) {
                mp = InputManager.getInstance().getMousePosition();
            }

            // Get norm toward mouse position
            Vector2f norm = new Vector2f(mp.getX() - pbc.getCenter().getX(), mp.getY() - pbc.getCenter().getY()).normalise();

            AttackComponent atc = e.getComponent(AttackComponent.class);
            DefenceComponent dc = e.getComponent(DefenceComponent.class);

            // Attack
            int attackKey = im.getKeyBindings().get(InputManager.Binds.PLAYER_ATTACK);
            if (ic.getKeyStates().get(attackKey)) {
                if (!atc.isAttacking() && !atc.isCoolingDown() && !dc.isDefending()) {

                    // Create weapon entity
                    Entity weapon = world.getManager(WeaponManager.class).addWeapon(WeaponComponent.WeaponType.SWORD, e, norm);
                    atc.setWeapon(weapon);
                    atc.setAttacking(true);

                    // Set orientation
                    attackOrientation = world.getManager(OrientationManager.class).getOrientationFromNorm(norm);
                }
            }

            // Defense
            int defenceKey = im.getKeyBindings().get(InputManager.Binds.PLAYER_DEFENCE);
            if (ic.getKeyStates().get(defenceKey)) {
                if(!atc.isAttacking()) {

                    // Set orientation
                    defenceOrientation = world.getManager(OrientationManager.class).getOrientationFromNorm(norm);

                    if (!dc.isDefending()) {
                        // Create shield entity
                        Entity shield = world.getManager(ShieldManager.class).addShield(e, norm);
                        dc.setShield(shield);
                        dc.setDefending(true);
                    }
                    else {
                        dc.getShield().getComponent(ShieldComponent.class).setDirection(norm);
                    }
                }
            }
            else {
                dc.setDefending(false);
            }

            // Update the orientation
            if(atc.isAttacking() || dc.isDefending()) {
                if(attackOrientation != null) {
                    oc.setOrientation(attackOrientation);
                }
                else if(defenceOrientation != null) {
                    oc.setOrientation(defenceOrientation);
                }
            }
            else if(moveOrientation != null) {
                oc.setOrientation(moveOrientation);
            }
        }
    }
}
