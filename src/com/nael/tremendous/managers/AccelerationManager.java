package com.nael.tremendous.managers;

import com.apollo.Entity;
import com.apollo.managers.EntityManager;
import com.apollo.managers.Manager;
import com.apollo.utils.Bag;
import com.nael.tremendous.components.AccelerationComponent;
import com.nael.tremendous.components.NetworkIdComponent;
import com.nael.tremendous.components.SpeedComponent;
import org.newdawn.slick.geom.Vector2f;
import org.newdawn.slick.util.Log;

public class AccelerationManager extends Manager {

    private Bag<Entity> dynamicsEntities;


    @Override
    public void initialize() {
        Log.info("[GAME] - Initializing AccelerationManager");
        dynamicsEntities = new Bag<Entity>();
    }

    @Override
    public void update(int delta) {
        EntityManager em = world.getEntityManager();
        dynamicsEntities = em.getEntitiesByComponentType(SpeedComponent.class);
    }

    public void processAccelerations(int delta) {
        float deltaAdjustValue = 2.f / delta;

        for(Entity e : dynamicsEntities) {
            AccelerationComponent ac = e.getComponent(AccelerationComponent.class);
            SpeedComponent speed = e.getComponent(SpeedComponent.class);
            speed.applyAccelerationX(ac.getAx() * deltaAdjustValue);
            speed.applyAccelerationY(ac.getAy() * deltaAdjustValue);

            // Negate acceleration once speed has been updated
            ac.setVector((new Vector2f(0f, 0f)));
        }
    }

    public void setAcceleration(Entity e, Vector2f a) {
        if(e != null && a != null) {
            e.getComponent(AccelerationComponent.class).setVector(a);
        }
    }
}
